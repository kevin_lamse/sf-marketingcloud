package org.ncaa.sfmc.committees;

import groovy.sql.Sql

import org.ncaa.sfmc.committees.entities.*;
import oracle.sql.CLOB;

/**
 Export of membership for SalesForce.  Will have duplicate records for a given email because a member can have multiple titles
 and a coach can have multiple sports.
 select * from SF_MEMBERSHIP
 Export of member sport tables. See query for this view.
 select * from SF_MEMBER_SPORTS
 */
class DBManager {

	def sql


	public DBManager(JobRunner jr) {
		sql = Sql.newInstance(
				jr.dbConnection,
				jr.dbUsername,
				jr.dbPassword)
	}

	public List getCommitteesView(String status) {
		List retList

		if(jr.testEnv)
			retList = this.getCommitteesViewTest(status)
		else
			retList = this.getCommitteesViewProd(status)
		return retList
	}
	//					def clobValue = (oracle.sql.CLOB)row.emailbody
	//					c.emailbody = clobValue?.asciiStream.text

	public List getCommitteesViewTest(String status) {
		List retList;
		if("A".contentEquals(status)) {
			retList = this.getCommitteesViewTestAll()
		}else if ("P".equals(status)) {
			retList = this.getCommitteesViewTestPartial()
		}
		//todo: throw excpetion if null
		return retList
	}

	public List getCommitteesViewProd(String status) {
		List retList;
		if("A".contentEquals(status)) {
			retList = this.getCommitteesViewProdAll()
		}else if ("P".equals(status)) {
			retList = this.getCommitteesViewProdPartial()
		}
		//todo: throw excpetion if null
		return retList
	}


	public List getCommitteesViewProdAll() {
		List retList = new ArrayList()

		//todo
		sql.eachRow(
				"""
				select email, emailbody, email_subject, email_from from COMMITTEESALESFORCEV
				""")
		{ row ->

			Committee c = new Committee()
			c.email = row.email
			c.emailbody = row.emailbody
			c.emailSubject = row.email_subject
			c.emailFrom = row.email_from

			retList.add(c)
		}

		return retList

	}

	public List getCommitteesViewProdPartial() {
		List retList = new ArrayList()

		//todo
		sql.eachRow(
				"""
				select email, emailbody, email_subject, email_from from COMMITTEESALESFORCEPV 
				""")
		{ row ->

			Committee c = new Committee()
			c.email = row.email
			c.emailbody = row.emailbody
			c.emailSubject = row.email_subject
			c.emailFrom = row.email_from

			retList.add(c)
		}

		return retList

	}

	public List getCommitteesViewTestAll() {
		List retList = new ArrayList()

		//todo
		sql.eachRow(
				"""
				select email, emailbody, email_subject, email_from from COMMITTEESALESFORCEV 
				""")
		{ row ->

			Committee c = new Committee()
			c.email = row.email
			c.emailbody = row.emailbody
			c.emailSubject = row.email_subject
			c.emailFrom = row.email_from

			retList.add(c)
		}

		return retList

	}

	public List getCommitteesViewTestPartial() {
		List retList = new ArrayList()

		//todo
		sql.eachRow(
				"""
				select email, emailbody, email_subject, email_from from COMMITTEESALESFORCEV 
				""")
		{ row ->

			Committee c = new Committee()
			c.email = row.email
			c.emailbody = row.emailbody
			c.emailSubject = row.email_subject
			c.emailFrom = row.email_from

			retList.add(c)
		}

		return retList

	}

	//CommitteeCoiEmail
	/**
	 * Should only be 1 row.  Exception if more.
	 * @return
	 */
	public List getCommitteeCoiEmailList() {
		List retList = new ArrayList()

		//todo
		sql.eachRow(
				"""
				select coi_email_id, status, sent_date from committee_coi_email where sent_date is null
				""")
		{ row ->

			CommitteeCoiEmail c = new CommitteeCoiEmail()
			c.coiEmailId = row.coi_email_id
			c.status = row.status
			c.sentDate = row.sent_date

			retList.add(c)
		}

		return retList

	}

	//COMMITTEE_COI_EMAIL.STATUS:  A = ALL, P = PARTIAL
	// ALL view =
	// Parial view =
	public void logSend(int count) {
		println "TODO: logSend sent file with " + count + " records!"
		try {
		sql.executeUpdate("update committee_coi_email set sent_date = sysdate, sent_count = ? where sent_date is null",[count])
		}catch (Exception e) {
			e.printStackTrace()
			jr.logfile.append "Error in logSend: "+e.message()
		}
	}

	public int getCommitteeViewCount() {
		println "getCommitteeViewCount()"
		int retVal = -1
		def s_ret = sql.rows(" select * from COMMITTEESALESFORCEV  ")
		retVal = s_ret.size();
		return retVal
	}


	public static void main(String[] args){
		DBManager dbc = new DBManager();
		println "test connection, studentCount = "+ dbc.getAnnouncementCount()
		dbc.getEmailList()
		println "done"
	}

}
