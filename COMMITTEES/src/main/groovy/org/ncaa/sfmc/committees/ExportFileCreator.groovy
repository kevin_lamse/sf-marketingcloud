package org.ncaa.sfmc.committees

import java.text.SimpleDateFormat
import org.ncaa.sfmc.committees.entities.*;


class ExportFileCreator {

	final String COMMITTEES_FILE_PREFIX = "COMMITTEES"
	final String EXPORT_DIR = "EXPORT"
	final String SUFFIX = ".csv"
	final String SUFFIX_TXT = ".txt"

	JobRunner jr

	public ExportFileCreator(JobRunner jr) {
		this.jr = jr
	}



	public String createCommitteesTabDelim(List list) {
		jr.logfile.append "ExportFileCreator.createCommitteesTabDelim() count="+list.size() +"\n"
		String fileName
		if(jr.testEnv) 	fileName = EXPORT_DIR+"_TEST/"+COMMITTEES_FILE_PREFIX+SUFFIX_TXT
		else			fileName = EXPORT_DIR+"/"+COMMITTEES_FILE_PREFIX+SUFFIX_TXT

		File f = new File(fileName)

		f.append("EMAIL\tEMAILBODY\tEMAIL_SUBJECT\tEMAIL_FROM\n")

		for(Committee c : list) {
			f.append(rv2(c.email)+"\t"+stripit(c.emailbody)+"\t"+rv2(c.emailSubject)+"\t"+rv2(c.emailFrom)+"\n")
		}

		return fileName
	}

	public void createAnnounceCSV() {
		println "createAnnounceCSV()"
	}
	//todo: add formatting?

	private String rvf(java.sql.Timestamp d) {
		String rval = ""

		if(d != null)
			rval = "\""+d.format("yyyy-MM-dd")+"\""
		return rval
	}
	private String rv(java.sql.Timestamp d) {
		String rval = ""

		if(d != null)
			rval = "\""+d+"\""
		return rval
	}

	private String rv(String s) {
		String rval = ""

		if(s != null)
			rval = "\""+ s +"\""
		return rval
	}

	private String rv(int s) {
		String rval = ""

		if(s != null)
			rval = "\""+ s +"\""
		return rval
	}

	private String rv2(String s) {
		String rval = ""

		if(s != null)
			rval = s
		return rval
	}

	private String stripit(String s) {
		String rval = ""

		if(s != null) {
			rval = s.replaceAll("\\n","")
			rval = rval.replaceAll("\\r","")
			rval = rval.replaceAll("\\t","")
		}
		return rval
	}

}
