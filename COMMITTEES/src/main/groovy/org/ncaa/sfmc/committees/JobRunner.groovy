package org.ncaa.sfmc.committees;

import org.ncaa.sfmc.committees.entities.*;
import groovy.util.logging.*;
//import org.apache.logging.log4j.Log4j2;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Send COMMITTEES.txt file to SF-Marketing Cloud's sftp site: /Triggered_Automations/COMMITTEES.
 *  If email end is type A (All), send COMMITTEESALESFORCEV
 *  If email send is type P (Partial), send COMMITTEESALESFORCEPV
 *  The file is sent if there is a null column in committee_coi_email.
 *  Note: committee_coi_email.send_date, and send_count are populated as a type of log of the send. 
 * @author klamse
 *
 */
//I don't know why sts shows a warning on this annotation, but it works. 
@Log4j2
class JobRunner extends JobRunnerConfig {


	boolean testEnv = false

	public JobRunner() {
	}

	public void sendEmail(String messageS){
		println "message= "+messageS
		ant.mail(
				mailhost:'localhost',
				mailport:'25',
				subject:'SalesForce Error',
				charset:'utf-8') {
					from(address:'klamse@ncaa.org')
					to(address:'klamse@ncaa.org')
					message(messageS)
				}
	}

	public static void main(String[] args) {
		JobRunner jr = new JobRunner()
		InetAddress localhost = null;
		try {
			println "JobRunner.main() for COMMITTEES"
			Date startD = new Date()
			String startDateS = startD.format("yyyyMMdd hh:mm:ss a")
			println "(JobRunner)this.createFile= " +jr.createFile
			println "(JobRunner)this.sendFile= " +jr.sendFile
			println "(JobRunner)this.showDebug= " +jr.showDebug

			if(args.size() > 0 && args[0].equals("TEST")) jr.testEnv = true

			jr.logfile.append "testEnv (from gradle) = "+jr.testEnv +"\n"
			//		NightlyFileManager nfm = new NightlyFileManagerCommittees()
			//
			jr.logfile.append("START: =============================================  \n")
			jr.logfile.append("START: "+startDateS +"\n")
			jr.logfile.append("START: =============================================  \n")

			ExportFileCreator efc = new ExportFileCreator(jr)

			DBManager dbm = new DBManager(jr)
			Sftp sftp = new Sftp(jr)
			List coiEmailList = dbm.getCommitteeCoiEmailList()
			int coiEmailListSize = coiEmailList.size()
			println "coiEmailListSize= "+coiEmailListSize
			switch ( coiEmailListSize ) {
				case 0:
					println "No emails to send "
					break
				case 1:
					println "Emails to send , status= " +coiEmailList.get(0).status
					List viewList = dbm.getCommitteesViewTest(coiEmailList.get(0).status)
					String fileCreated = efc.createCommitteesTabDelim(viewList)
					sftp.sendFile(fileCreated)
					dbm.logSend(viewList.size())
					println "Emails sent "+viewList.size()
					break
				default:
					println "Too many emails (Error) "
					localhost = InetAddress.getLocalHost();
					throw new Exception("Too many records in committee_coi_email with send_date null.")
					break
			}

			Date endD = new Date()
			jr.logfile.append("END: "+endD.format("yyyyMMdd hh:mm:ss a") +"\n")
			jr.logfile.append("======================================")
		} catch (Exception e) {
			e.printStackTrace()
			println "localhost ="+localhost.getHostName()
			if("Kevins-MBP.ncaa.org".equals(localhost.getHostName())) 
				println "On mac, so don't sendEmail() with " +e.getMessage()
			else
				jr.sendEmail("Cronjob-COMMITTEES Error: "+e.getMessage())
			
		}
	}
}
