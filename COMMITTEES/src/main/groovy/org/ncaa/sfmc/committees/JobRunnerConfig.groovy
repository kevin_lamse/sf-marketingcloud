package org.ncaa.sfmc.committees;

import java.io.File;

class JobRunnerConfig {

	final String LOG_DIR = "logs/"
	String LOG_FILE = "COMMITTEES";
	protected File logfile
	//File log
	String dateFormatted

	boolean createFile = true
	boolean sendFile = true
	boolean showDebug = true
	String toDir
	String password
	AntBuilder ant


	String dbConnection
	String dbUsername
	String dbPassword

	public JobRunnerConfig() {
		setProperties()
		createLogfile()
		ant = new AntBuilder() //do I need so many?
		logfile.append "\nJobRunnerConfig() initialized \n"
	}

	private void createLogfile() {
		Date d = new Date()
		dateFormatted = d.format("yyyyMMdd")
		println "dateFormatted= " + dateFormatted
		logfile = new File(this.LOG_DIR+this.LOG_FILE+"-"+dateFormatted+".log")
		log.info ("(log.info)start at {} ",dateFormatted) // does this work?

	}

	private void setProperties() {
		String propFile = 'src/main/resources/job.properties'
		println "JobRunnerConfig.setProperties() from " +propFile
		Properties properties = new Properties()
		File propertiesFile = new File(propFile)
		propertiesFile.withInputStream { properties.load(it) }

		if(properties."salesforce.committees.createFile")
			this.createFile = properties."salesforce.committees.createFile".toBoolean()
		println "(JobRunnerConfig)this.createFile = "+this.createFile

		if(properties."salesforce.committees.sendFile")
			this.sendFile = properties."salesforce.committees.sendFile".toBoolean()
		println "(JobRunnerConfig)this.sendFile = "+this.sendFile

		if(properties."salesforce.committees.showDebug")
			this.showDebug = properties."salesforce.committees.showDebug".toBoolean()
		println "(JobRunnerConfig)this.showDebug = "+this.showDebug

		if(properties."salesforce.sftp.url")
			this.toDir = properties."salesforce.sftp.url"
		println "(JobRunnerConfig) this.showDebug = "+this.toDir

		if(properties."salesforce.sftp.password")
			this.password= properties."salesforce.sftp.password"
		println "(JobRunnerConfig) this.password = "+this.password

		//Load another file
		propFile = 'src/main/resources/jdbc.properties'
		println "JobRunnerConfig.setProperties() from " +propFile
		propertiesFile = new File(propFile)
		propertiesFile.withInputStream { properties.load(it) }
		if(properties."ncaa.dbConnection")
			this.dbConnection = properties."ncaa.dbConnection"
		println "(JobRunnerConfig) this.dbConnection = "+this.dbConnection

		if(properties."ncaa.dbUsername")
			this.dbUsername = properties."ncaa.dbUsername"
		println "(JobRunnerConfig) this.dbUsername = "+this.dbUsername

		if(properties."ncaa.dbPassword")
			this.dbPassword = properties."ncaa.dbPassword"
		//println "(JobRunnerConfig) this.dbPassword = "+this.dbPassword

	}

}
