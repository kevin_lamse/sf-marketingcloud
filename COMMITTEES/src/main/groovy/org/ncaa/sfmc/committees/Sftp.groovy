package org.ncaa.sfmc.committees

import org.ncaa.sfmc.committees.JobRunner

class Sftp {

	JobRunner jr
	String toDir
	String password

	public Sftp(JobRunner jr) {
		println "Sftp() initialized"
		this.jr = jr
	}

	public void sendFile(String fileToSend ) {
		jr.logfile.append " ->Sftp.sendFile "+ fileToSend
		String toDirFinal
		if(jr.testEnv) toDirFinal = jr.toDir + "_TEST"
		else toDirFinal = jr.toDir
		println " ->sending to: "+toDirFinal
		jr.ant.scp(
				file: fileToSend,
				todir: toDirFinal,
				password: jr.password,
				sftp: true,
				trust: true,
				verbose: true
				)
	}
}
