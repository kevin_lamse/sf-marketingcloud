package org.ncaa.sfmc.committees

class SftpManager {

	def ant
	String toDir
	String password
	
	//new
	public SftpManager() {
		println "SftpManger()"
		ant = new AntBuilder()
		this.toDir = "10891955@ftp1.exacttarget.com:Import/Membership"
		this.password = "4Tb+!8Ne5g%QaB9.*3"		
	}
	
	public SftpManager(Properties props) {
		println "SftpManger(Properties props) Constructor"
		ant = new AntBuilder()
		this.toDir = props."salesforce.sftp.url"
		this.password = props."salesforce.sftp.password"
	}
	
	public void sendFile(String fileToSend ) {
		
		ant.scp(
			file: fileToSend,
			todir: this.toDir,
			password: this.password,
			sftp: true,
			trust: true,
			verbose: true
			) 
			
	}
	
	public void sendFile(String fileToSend, String toDir ) {
		
		ant.scp(
			file: fileToSend,
			todir: toDir,
			password: this.password,
			sftp: true,
			trust: true,
			verbose: true
			)
			
	}
	
	public static void main(String[] args) {
		SftpManager sm = new SftpManager()
		sm.sendFile("EXPORT/TEST.txt")	
	}
}
