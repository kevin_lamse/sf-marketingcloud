package org.ncaa.sfmc.committees.entities;

import groovy.transform.ToString;

@ToString
public class Committee {

	String email
	String emailbody
	String emailSubject
	String emailFrom
	
}