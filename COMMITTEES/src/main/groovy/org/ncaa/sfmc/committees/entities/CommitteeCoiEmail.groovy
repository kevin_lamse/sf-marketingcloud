package org.ncaa.sfmc.committees.entities

import groovy.transform.ToString;

@ToString
class CommitteeCoiEmail {

	Long coiEmailId
	String status
	Date sentDate
}
