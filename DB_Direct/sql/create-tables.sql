
/**
 * 
 */
create table SF_MARKETING_EMAILS (
	ORACLE_TABLE	VARCHAR2(30), -- append _TEST for test.  Include schema name if necessary. 
	SF_FILE_UPLOAD_DIR	VARCHAR2(200), -- ADD THIS TO SFTP URL, append _TEST for test
	hour_to_process	number, 	-- 1 - 24:  (24 is midnight) can process up to every hour. 
	ACTIVE 			VARCHAR2(1) -- T/F
)

SELECT ORACLE_TABLE, SF_FILE_UPLOAD_DIR, hour_to_process, ACTIVE FROM SF_MARKETING_EMAILS

select 

INSERT INTO SF_MARKETING_EMAILS(ORACLE_TABLE,SF_FILE_UPLOAD_DIR,hour_to_process,ACTIVE) VALUES ('TEST_TABLE','TEST_FILE_DIR',1,'T')

INSERT INTO SF_MARKETING_EMAILS(ORACLE_TABLE,SF_FILE_UPLOAD_DIR,hour_to_process,ACTIVE) VALUES ('TEST_TABLE2','TEST_FILE_DIR2',2,'T')
