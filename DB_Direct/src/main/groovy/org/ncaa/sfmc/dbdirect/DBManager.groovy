package org.ncaa.sfmc.dbdirect;

import groovy.sql.Sql
import java.sql.ResultSet
import java.sql.ResultSetMetaData

import org.ncaa.sfmc.dbdirect.JobRunner
import org.ncaa.sfmc.dbdirect.JobRunnerConfig;
import org.ncaa.sfmc.dbdirect.entities.*;

import oracle.sql.CLOB;

/**
 	Access SF_MARKETING_EMAILS every hour to determine which scripts need to be run.
 	
 */
class DBManager {

	def sql
	JobRunnerConfig jrc
	
	public final String SF_MARKETING_EMAILS = "SF_MARKETING_EMAILS"
	
	/** 
	 * DB connection based on test or prod parameter sent to to job. 
	 * @param jr
	 */
	public DBManager(JobRunnerConfig jrc) {
		this.jrc = jrc
		sql = Sql.newInstance(
				jrc.dbConnection,
				jrc.dbUsername,
				jrc.dbPassword)
	}


	public List getSFMarketingEmailsList() {
		List retList = new ArrayList()
		
		println "sql="+sql
		
		sql.eachRow(
			"""
				SELECT ORACLE_TABLE, SF_FILE_UPLOAD_DIR, hour_to_process, ACTIVE FROM SF_MARKETING_EMAILS
			""")
	{ row ->

		SFMarketingEmails emails = new SFMarketingEmails()
		emails.oracleTable = row.ORACLE_TABLE
		emails.sfFileUploadDir = row.SF_FILE_UPLOAD_DIR
		emails.hourToProcess = row.hour_to_process
		emails.active = row.active

		retList.add(emails)
	}

	}
	
	public void getSFMarketingEmailsList2() {
		
		
		ResultSet rs = sql.executeQuery("SELECT ORACLE_TABLE, SF_FILE_UPLOAD_DIR, hour_to_process, ACTIVE FROM SF_MARKETING_EMAILS")
		println "rs = "+rs
/*		
		ResultSetMetaData rsmd = rs.getMetaData()
		int c = rsmd.getColumnCount()
		println "count = "+c
	*/	
		//Connection c = 
		println "="
		try {
		java.sql.Connection c = sql.getConnection()
		println "="
		java.sql.Statement s = c.createStatement()
		println "="
		java.sql.ResultSet rs2 = c.executeQuery("select * from SF_MARKETING_EMAILS")
		println "="
		java.sql.ResultSetMetaData rsmd = rs2.getMetaData()
		println "colcount = "+rsmd.getColumnCount()
		println " "+ rsmd.getColumnName(0)
		
		} catch (Exception e) {
			e.printStackTrace()
		}
	}
			
	//					def clobValue = (oracle.sql.CLOB)row.emailbody
	//					c.emailbody = clobValue?.asciiStream.text

	public void logSend(int count) {
		println "TODO: logSend sent file with " + count + " records!"
		try {
			
		}catch (Exception e) {
			e.printStackTrace()
			jr.logfile.append "Error in logSend: "+e.message()
		}
	}
	
	public int getCommitteeViewCount() {
		println "getCommitteeViewCount()"
		int retVal = -1
		def s_ret = sql.rows(" select * from COMMITTEESALESFORCEV  ")
		retVal = s_ret.size();
		return retVal
	}


	public static void main(String[] args){
		DBManager dbc = new DBManager();
		println "test connection, studentCount = "+ dbc.getAnnouncementCount()
//		dbc.getEmailList()
		println "done"
	}

}
