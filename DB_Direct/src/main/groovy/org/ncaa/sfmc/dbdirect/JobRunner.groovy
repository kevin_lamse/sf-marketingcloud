package org.ncaa.sfmc.dbdirect;

import org.ncaa.sfmc.dbdirect.DBManager;
import org.ncaa.sfmc.dbdirect.ExportFileCreator;
import org.ncaa.sfmc.dbdirect.JobRunner;
import org.ncaa.sfmc.dbdirect.JobRunnerConfig;
import org.ncaa.sfmc.dbdirect.Sftp;

import groovy.util.logging.*;

/**
 * Create announcement csv file for LSDBi accouncements.
 * Create MEMBER and MEMBER_SPORT files for SalesForce DB of membership tables.
 * 
 * 
 * @author klamse
 *
 */
@Log4j2
class JobRunner {


	boolean testEnv = false
	JobRunnerConfig jrc
	
	/**
	 * Put config in JobRunnerConfig to keep things clean.
	 * JobRunner goes high level processing.
	 * @param env
	 */
	public JobRunner(env) {
		this.jrc = new JobRunnerConfig(env)
	}

	public static void main(String[] args) {
		try {
		println "JobRunner.main() for DB_Direct"
		String p_env
		if(args.size() > 0 && args[0].equals("test") ){
			p_env = "test"
		} else if (args.size() > 0 && args[0].equals("prod") ){
			throw new Exception("prod not configured yet")
			p_env = "test"
		} else {
			throw new Exception("Need Env 'prod' or 'test'")
		}
		
		Date startD = new Date()

		JobRunner jr = new JobRunner(p_env)

		jr.jrc.logfile.append("START: =============================================  \n")
		jr.jrc.logfile.append("START: "+startD.format("yyyyMMdd hh:mm:ss a") +"\n")
		jr.jrc.logfile.append("START: =============================================  \n")

		//worker classes
		ExportFileCreator efc = new ExportFileCreator(jr.jrc)
		DBManager dbm = new DBManager(jr.jrc)
		Sftp sftp = new Sftp(jr.jrc)

		List viewList = dbm.getSFMarketingEmailsList()
			
		//resultSet
		dbm.getSFMarketingEmailsList2()
		viewList.each {
			println "e = ${it}"
		}
		
		/*
		String fileCreated = efc.createCommitteesTabDelim(viewList)
		sftp.sendFile(fileCreated)
		dbm.logSend(viewList.size())
		*/
		
		Date endD = new Date()
		jr.jrc.logfile.append("END: "+endD.format("yyyyMMdd hh:mm:ss a") +"\n")
		jr.jrc.logfile.append("======================================")
		} catch (Exception e){
			println "Exception= "+e.getMessage()
			e.printStackTrace()
		}
	}
}
