package org.ncaa.sfmc.dbdirect

import java.io.File

import groovy.util.logging.Log4j2;

@Log4j2
class JobRunnerConfig {

	final String LOG_DIR = "logs/"
	String LOG_FILE = "SF_LOG";
	protected File logfile
	//File log
	String dateFormatted
	String configFile
	
	boolean createFile = true
	boolean sendFile = true
	boolean showDebug = true
	String viewName
	
	String toDir
	String password
	AntBuilder ant


	String dbConnection
	String dbUsername
	String dbPassword

	//env - prod or test
	public JobRunnerConfig(String env) {
		setProperties(env)
		createLogfile(env)
		ant = new AntBuilder() //do I need so many?
		logfile.append "\nJobRunnerConfig() initialized \n"
	}

	private void createLogfile(String env) {
		Date d = new Date()
		dateFormatted = d.format("yyyyMMdd")
		println "dateFormatted= " + dateFormatted
		logfile = new File(this.LOG_DIR+env+"-"+this.LOG_FILE+"-"+dateFormatted+".log")
		log.info ("(log.info)start at {} ",dateFormatted) // does this work?

	}

	private void setProperties(String env) {
		String propFile = "src/main/resources/job-"+env+".properties"
		println "JobRunnerConfig.setProperties() from " +propFile
		Properties properties = new Properties()
		File propertiesFile = new File(propFile)
		propertiesFile.withInputStream { properties.load(it) }

		//configFile
		if(properties."salesforce.configFile")
			this.configFile = properties."salesforce.configFile"
		println "(JobRunnerConfig)this.createFile = "+this.configFile
		println "propertiesFile = "+propertiesFile

		if(properties."salesforce.createFile")
			this.createFile = properties."salesforce.createFile".toBoolean()
		println "(JobRunnerConfig)this.createFile = "+this.createFile

		if(properties."salesforce.sendFile")
			this.sendFile = properties."salesforce.sendFile".toBoolean()
		println "(JobRunnerConfig)this.sendFile = "+this.sendFile

		if(properties."salesforce.showDebug")
			this.showDebug = properties."salesforce.showDebug".toBoolean()
		println "(JobRunnerConfig)this.showDebug = "+this.showDebug

		if(properties."salesforce.viewName")
		this.viewName = properties."salesforce.viewName"
		println "(JobRunnerConfig)this.viewName = "+this.viewName

		if(properties."salesforce.sftp.url")
			this.toDir = properties."salesforce.sftp.url"
		println "(JobRunnerConfig) this.showDebug = "+this.toDir

		if(properties."salesforce.sftp.password")
			this.password= properties."salesforce.sftp.password"

		//Load another file
		propFile = "src/main/resources/jdbc-"+env+".properties"
		println "JobRunnerConfig.setProperties() from " +propFile
		propertiesFile = new File(propFile)
		propertiesFile.withInputStream { properties.load(it) }
		if(properties."ncaa.dbConnection")
			this.dbConnection = properties."ncaa.dbConnection"
		println "(JobRunnerConfig) this.dbConnection = "+this.dbConnection

		if(properties."ncaa.dbUsername")
			this.dbUsername = properties."ncaa.dbUsername"
		println "(JobRunnerConfig) this.dbUsername = "+this.dbUsername

		if(properties."ncaa.dbPassword")
			this.dbPassword = properties."ncaa.dbPassword"

	}

}
