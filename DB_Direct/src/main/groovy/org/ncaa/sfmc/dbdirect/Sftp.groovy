package org.ncaa.sfmc.dbdirect

import org.ncaa.sfmc.dbdirect.JobRunner;;;

class Sftp {

	JobRunnerConfig jrc
	String toDir
	String password

	public Sftp(JobRunnerConfig jrc) {
		println "Sftp() initialized"
		this.jrc = jrc
	}

	public void sendFile(String fileToSend ) {
		jrc.logfile.append " ->Sftp.sendFile "+ fileToSend
		String toDirFinal //todo
		
		println " ->sending to: "+toDirFinal
		jrc.ant.scp(
				file: fileToSend,
				todir: toDirFinal,
				password: jrc.password,
				sftp: true,
				trust: true,
				verbose: true
				)
	}
}
