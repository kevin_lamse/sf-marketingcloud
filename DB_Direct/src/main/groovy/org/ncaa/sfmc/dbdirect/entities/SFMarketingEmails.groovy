package org.ncaa.sfmc.dbdirect.entities

import groovy.transform.ToString;

@ToString
class SFMarketingEmails {

	String 		oracleTable //match file name
	String 		sfFileUploadDir
	String		hourToProcess
	String 		active

	
}
