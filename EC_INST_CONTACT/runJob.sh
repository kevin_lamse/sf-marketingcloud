
#!/bin/bash

args=("$@")
c=("$#")
if [ $c -lt 1 ]; then
        echo "Please specify: local, dev3, or prod"
fi

if [ ${args[0]} = "local" ]; then
        echo "run local..."
        export CRONJOB_BASEDIR=/Users/klamse/IdeaProjects/sf-marketingcloud/EC_INST_CONTACT
        cd $CRONJOB_BASEDIR
		export RUN_JOB_ENV=test
        . ../setenv-local.sh

elif [ ${args[0]} = "prodtest" ]; then
        echo "run dev3..."
        export CRONJOB_BASEDIR=/data/cronjobs/SF-MarketingCloud/TEMPLATE
        cd $CRONJOB_BASEDIR
		export RUN_JOB_ENV=test
        . ../setenv-prodtest.sh

elif [ ${args[0]} = "prod" ]; then
        echo "run prod..."
        export CRONJOB_BASEDIR=/data/cronjobs/SF-MarketingCloud/EC_INST_CONTACT
        cd $CRONJOB_BASEDIR
		export RUN_JOB_ENV=prod
        . ../setenv-prod.sh

fi

gradle runJob --stacktrace
