package org.ncaa.sfmc.eic;

import groovy.sql.Sql;


import org.ncaa.sfmc.eic.entities.*;

import oracle.sql.CLOB;

/**

 */
class DBManager {

	def sql
	def jrc

	public DBManager(JobRunnerConfig jrc) {
		this.jrc = jrc
		sql = Sql.newInstance(
				jrc.dbConnection,
				jrc.dbUsername,
				jrc.dbPassword)
	}

		
	public List getView() {
		List retList = new ArrayList()

		String sqlS = "select distinct email INST_CONTACT_EMAIL from inst_contact where email is not NULL"
		println "SQL = "+sqlS
		sql.eachRow(sqlS)
		{ row ->

			SalesForceExport sfe = new SalesForceExport()
			sfe.INST_CONTACT_EMAIL 		= row.INST_CONTACT_EMAIL

			retList.add(sfe)
			
		}
		return retList

	}

	public void logSend(int count) {
		println "TODO: logSend sent file with " + count + " records!"
		try {
			
		}catch (Exception e) {
			e.printStackTrace()
			jrc.logfile.append "Error in logSend: "+e.message()
		}
	}
	
/* Example
	public int getCommitteeViewCount() {
		println "getCommitteeViewCount()"
		int retVal = -1
		def s_ret = sql.rows(" select * from COMMITTEESALESFORCEV  ")
		retVal = s_ret.size();
		return retVal
	}
*/

	public static void main(String[] args){
		DBManager dbc = new DBManager();
		println "done"
	}

}
