package org.ncaa.sfmc.eic;

import java.text.SimpleDateFormat

import org.ncaa.sfmc.eic.JobRunner;
import org.ncaa.sfmc.eic.JobRunnerConfig;
import org.ncaa.sfmc.eic.entities.*;


class ExportFileCreator {


	JobRunnerConfig jrc

	public ExportFileCreator(JobRunnerConfig jrc) {
		this.jrc = jrc
	}



	public String createExportFile(List list) {
		jrc.logfile.append "ExportFileCreator.createCommitteesTabDelim() count="+list.size() +"\n"
		String fileName = jrc.exportDir+ "/"+jrc.fileName
		
		File f = new File(fileName)

		f.append("INST_CONTACT_EMAIL\n")

		for(SalesForceExport e: list) {
			f.append(rv(e.INST_CONTACT_EMAIL)+"\n" )
		}
		
		return fileName
	}


	private String rvf(java.sql.Timestamp d) {
		String rval = ""

		if(d != null)
			rval = "\""+d.format("yyyy-MM-dd")+"\""
		return rval
	}
	private String rv(java.sql.Timestamp d) {
		String rval = ""

		if(d != null)
			rval = "\""+d+"\""
		return rval
	}

	private String rv(String s) {
		String rval = ""

		if(s != null)
			rval = "\""+ s +"\""
		return rval
	}

	private String rv(int s) {
		String rval = ""

		if(s != null)
			rval = "\""+ s +"\""
		return rval
	}

	private String rv2(String s) {
		String rval = ""

		if(s != null)
			rval = s
		return rval
	}

	private String stripit(String s) {
		String rval = ""

		if(s != null) {
			rval = s.replaceAll("\\n","")
			rval = rval.replaceAll("\\r","")
			rval = rval.replaceAll("\\t","")
			rval = "\""+ rval+ "\""
		}
		return rval
	}

}
