package org.ncaa.sfmc.eic;

import org.ncaa.sfcm.eic.entities.*;

import groovy.util.logging.*;

/**
 * Host Reporting Export
 * 
 * @author klamse
 *
 */
@Log4j2
class JobRunner {


	JobRunnerConfig jrc
	boolean testEnv = false

	public JobRunner() {
		this.jrc = new JobRunnerConfig()
	}


	public static void main(String[] args) {
		println "JobRunner.main() for TEMPLATE"
		
		Date startD = new Date()

		JobRunner jr = new JobRunner()
		
		jr.jrc.logfile.append("START: =============================================  \n")
		jr.jrc.logfile.append("START: "+startD.format("yyyyMMdd hh:mm:ss a") +"\n")
		jr.jrc.logfile.append("START: =============================================  \n")

		ExportFileCreator efc = new ExportFileCreator(jr.jrc)

		println "TODO: implement logic" 
		DBManager dbm = new DBManager(jr.jrc)
		Sftp sftp = new Sftp(jr.jrc)

		List viewList = dbm.getView();
		println "viewList.size = " +viewList.size()
		String fileCreated = efc.createExportFile(viewList)

		println "FileCreated = "+fileCreated
//
		sftp.sendFile(fileCreated)
		/*
		dbm.logSend(viewList.size())
		*/
		Date endD = new Date()
		jr.jrc.logfile.append("END: "+endD.format("yyyyMMdd hh:mm:ss a") +"\n")
		jr.jrc.logfile.append("======================================")

	}
}
