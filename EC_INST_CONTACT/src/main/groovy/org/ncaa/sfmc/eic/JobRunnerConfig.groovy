package org.ncaa.sfmc.eic;

import java.io.File;

class JobRunnerConfig {

	final String LOG_DIR = "logs/"
	String LOG_FILE = "SF_LOG";
	protected File logfile
	//File log
	String dateFormatted

	boolean createFile = true
	boolean sendFile = true
	boolean showDebug = true
	String viewName
	String configFile
	String env
	
	String exportDir
	String fileName
	String sftpURL
	String password
	AntBuilder ant


	String dbConnection
	String dbUsername
	String dbPassword

	//env - prod or test (not actually using for this deployment
	public JobRunnerConfig(String env) {
		println "RUN_JOB_ENV ="+ System.getenv("RUN_JOB_ENV")
		env = System.getenv("RUN_JOB_ENV")
		setProperties(env)
		createLogfile(env)
		ant = new AntBuilder() //do I need so many?
		logfile.append "\nJobRunnerConfig() initialized \n"
	}

	private void createLogfile(String env) {
		Date d = new Date()
		dateFormatted = d.format("yyyyMMdd")
		println "dateFormatted= " + dateFormatted
		logfile = new File(this.LOG_DIR+env+"-"+this.LOG_FILE+"-"+dateFormatted+".log")
//		log.info ("(log.info)start at {} ",dateFormatted) // does this work?

	}

	private void setProperties(String env) {
		String propFile = "src/main/resources/job.properties"
		println "JobRunnerConfig.setProperties() from " +propFile
		Properties properties = new Properties()
		File propertiesFile = new File(propFile)
		propertiesFile.withInputStream { properties.load(it) }

		//configFile
//		if(properties."salesforce.configFile")
//			this.configFile = properties."salesforce.configFile"
//		println "(JobRunnerConfig)this.createFile = "+this.configFile
//		println "propertiesFile = "+propertiesFile

		if(properties."sfmc.createFile")
			this.createFile = properties."sfmc.createFile".toBoolean()
		println "(JobRunnerConfig)this.createFile = "+this.createFile

		if(properties."sfmc.sendFile")
			this.sendFile = properties."sfmc.sendFile".toBoolean()
		println "(JobRunnerConfig)this.sendFile = "+this.sendFile

		if(properties."sfmc.showDebug")
			this.showDebug = properties."sfmc.showDebug".toBoolean()
		println "(JobRunnerConfig)this.showDebug = "+this.showDebug

		if(properties."sfmc.exportDir")
			this.exportDir = properties."sfmc.exportDir"
		println "(JobRunnerConfig)this.exportDir = "+this.exportDir

		if(properties."sfmc.fileName")
			this.fileName = properties."sfmc.fileName"
		println "(JobRunnerConfig)this.fileName = "+this.fileName

		if(properties."sfmc.sftp.url")
			this.sftpURL = properties."sfmc.sftp.url"
		println "(JobRunnerConfig) this.sftpURL = "+this.sftpURL

		if(properties."sfmc.sftp.password")
			this.password= properties."sfmc.sftp.password"


		//Load another file
		propFile = "src/main/resources/jdbc.properties"
		println "JobRunnerConfig.setProperties() from " +propFile
		propertiesFile = new File(propFile)
		propertiesFile.withInputStream { properties.load(it) }
		if(properties."ncaa.dbConnection")
			this.dbConnection = properties."ncaa.dbConnection"
		println "(JobRunnerConfig) this.dbConnection = "+this.dbConnection

		if(properties."ncaa.dbUsername")
			this.dbUsername = properties."ncaa.dbUsername"
		println "(JobRunnerConfig) this.dbUsername = "+this.dbUsername

		if(properties."ncaa.dbPassword")
			this.dbPassword = properties."ncaa.dbPassword"

	}

}
