package org.ncaa.sfmc.eic;

import org.ncaa.sfmc.eic.JobRunnerConfig;

class Sftp {

	JobRunnerConfig jrc
	String toDir
	String password

	public Sftp(JobRunnerConfig jrc) {
		println "Sftp() initialized"
		this.jrc = jrc
	}

	public void sendFile(String fileToSend ) {
		jrc.logfile.append " ->Sftp.sendFile "+ fileToSend
		println " ->sending to: "+jrc.sftpURL
		println " ->sendFile: "+jrc.sendFile
		if(jrc.sendFile) {
			jrc.ant.scp(
				file: fileToSend,
				todir: jrc.sftpURL,
				password: jrc.password,
				sftp: true,
				trust: true,
				verbose: true
				)
		}
	}
}
