package org.ncaa.sfmc.eic.entities

/**
 * Configure this class as needed for the export.
 * @author klamse
 *
 */
class SalesForceExport {

	String		INST_CONTACT_EMAIL
	
}
