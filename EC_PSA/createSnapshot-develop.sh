#!/bin/bash

args=("$@")
c=("$#")
if [ $c -lt 1 ]; then
	echo "Please specify: local, test, or prod"
fi

if [ ${args[0]} = "local" ]; then
	echo "run local..."
	export CRONJOB_BASEDIR=/Users/klamse/IdeaProjects/sf-marketingcloud/EC_PSA
	cd $CRONJOB_BASEDIR
	. ../setenv-local.sh

elif [ ${args[0]} = "dev3" ]; then
        echo "run dev3..."
        export CRONJOB_BASEDIR=/data/cronjobs/SF-MarketingCloud-develop/EC_PSA
        cd $CRONJOB_BASEDIR
        . ../setenv-prod.sh

elif [ ${args[0]} = "test" ]; then
	echo "run test..."
	export CRONJOB_BASEDIR=/data/cronjobs/SF-MarketingCloud-develop/EC_PSA
	cd $CRONJOB_BASEDIR
	. ../setenv-prod.sh

elif [ ${args[0]} = "prod" ]; then
	echo "run prod..."
	export CRONJOB_BASEDIR=/data/cronjobs/SF-MarketingCloud/EC_PSA
	cd $CRONJOB_BASEDIR
	. ../setenv-prod.sh

fi

gradle createSnapshot --stacktrace
