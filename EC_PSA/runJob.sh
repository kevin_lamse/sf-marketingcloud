#!/bin/bash

args=("$@")
c=("$#")
if [ $c -lt 1 ]; then
	echo "Please specify: local, dev3, or prod"
fi

if [ ${args[0]} = "local" ]; then
	echo "run local..."
	export CRONJOB_BASEDIR=/Users/klamse/dev/ws-experiment/SF-MarketingCloud/EC_PSA
	cd $CRONJOB_BASEDIR
	. ../setenv-local.sh

elif [ ${args[0]} = "dev3" ]; then
	echo "run dev3..."
	export CRONJOB_BASEDIR=/data/cronjobs/SF-MarketingCloud/EC_PSA
	cd $CRONJOB_BASEDIR
	. ../setenv-dev3.sh

elif [ ${args[0]} = "prod" ]; then
	echo "run prod..."
	export CRONJOB_BASEDIR=/data/cronjobs/SF-MarketingCloud/EC_PSA
	cd $CRONJOB_BASEDIR
	. ../setenv-prod.sh

fi

gradle runJob --stacktrace
