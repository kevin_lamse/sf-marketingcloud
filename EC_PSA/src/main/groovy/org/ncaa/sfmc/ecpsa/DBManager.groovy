package org.ncaa.sfmc.ecpsa;

import groovy.sql.Sql

import org.ncaa.sfmc.ecpsa.entities.EcPsaExport
import org.ncaa.sfmc.ecpsa.DBManager;
import org.ncaa.sfmc.ecpsa.JobRunner;

import oracle.sql.CLOB;

/**
 Export of membership for SalesForce.  Will have duplicate records for a given email because a member can have multiple titles
 and a coach can have multiple sports.
 select * from SF_MEMBERSHIP
 Export of member sport tables. See query for this view.
 select * from SF_MEMBER_SPORTS
v2
 */
class DBManager {

	def sql
	JobRunnerConfig jr

	public final String SALES_FORCE_EC = "SALES_FORCE_EC"
	public final String SALES_FORCE_EC_TEST = "SALES_FORCE_EC_TEST"
	public final String SALES_FORCE_EC_MODS = "SALES_FORCE_EC_MODS"
	public final String SALES_FORCE_EC_MODS_TEST = "SALES_FORCE_EC_MODS_TEST"
	public final String SALES_FORCE_EC_DELETES = "SALES_FORCE_EC_DELETES"
	public final String SALES_FORCE_EC_DELETES_TEST = "SALES_FORCE_EC_DELETES_TEST"


	public DBManager(JobRunnerConfig jr) {
		this.jr = jr
        if(jr.useProdDB) {
            sql = Sql.newInstance(
	                jr.dbConnection,
	                jr.dbUsername,
	                jr.dbPassword)
        } else {
            sql = Sql.newInstance(
                    jr.test_dbConnection,
                    jr.test_dbUsername,
                    jr.test_dbPassword)
        }
	}

	
	public List getSalesForceECMods() {
		println "getSalesForceEcMods"
		List retList = new ArrayList()

		String modsView = 	jr.useProdDBTables ? 
							this.SALES_FORCE_EC_MODS : 
							this.SALES_FORCE_EC_MODS_TEST

		String sqlString = 
		"""
	SELECT
        NCS_FOLDER,
		FIRST_NAME,
		LAST_NAME,
        SRF_DATE,
        EMAIL,
        AM_ENROLL_ID,
        ENROLL_SEMESTER,
        ENROLL_YEAR,
        OPEN_101_FORM,
        INTL_FLAG,
        home_school_staff,
        split_flag,
        cdn_flag,
        have_test_score,
        graduation_date,
        ready_to_process,
        min_task_date,
        max_task_date,
        sport_code,
        request_final_date,
        div1_decision_date,
        div1_sport_decision_id,
        div2_decision_date,
        div2_sport_decision_id,
        internal_sport_decision_id,
        recruit_cycle,
        cert_type_div1,
        cert_type_div2,
        pe_div1,
        pe_div2,
        payment_status,
		added_date,
		account_type,	
		file_stat,
		ORIGINAL_PRELIM_DATE_DIV1, ORIGINAL_FINAL_DATE_DIV1,
		COUNTRY
	FROM
        ${modsView}
        """
		sql.eachRow(sqlString) { row ->
			EcPsaExport a = new EcPsaExport()
			a.ncsFolder     		= row.ncs_folder
			a.firstName				= row.first_name
			a.lastName				= row.last_name
			a.srfDate               = row.srf_date
			a.email                 = row.email
			a.amEnrollId    		= row.am_enroll_id
			a.enrollSemester		= row.enroll_semester
			a.enrollYear    		= row.enroll_year
			a.open101Form   		= row.open_101_form
			a.intlFlag              = row.intl_flag
			a.homeSchoolStaff 	= row.home_school_staff
			a.splitFlag             = row.split_flag
			a.cdnflag               = row.cdn_flag
			a.haveTestScore 		= row.have_test_score
			a.graduationDate		= row.graduation_date
			a.readyToProcess		= row.ready_to_process
			a.minTaskDate   		= row.min_task_date
			a.maxTaskDate   		= row.max_task_date
			a.sportCode             = row.sport_code
			a.requestFinalDate		= row.request_final_date
			a.div1DecisionDate		= row.div1_decision_date
			a.div1SportDecisionId 	= row.div1_sport_decision_id
			a.div2DecisionDate 		= row.div2_decision_date
			a.div2SportDecisionId 	= row.div2_sport_decision_id
			a.internalSportDecisionId = row.internal_sport_decision_id
			a.recruitCycle  		= row.recruit_cycle
			a.certTypeDiv1 			= row.cert_type_div1
			a.certTypeDiv2 			= row.cert_type_div2
			a.peDiv1        		= row.pe_div1
			a.peDiv2        		= row.pe_div2
			a.paymentStatus 		= row.payment_status
			a.addedDate 			= row.added_date
			a.accountType   		= row.account_type
			a.fileStat			= row.file_stat
			a.originalPrelimDateDiv1 = row.ORIGINAL_PRELIM_DATE_DIV1
			a.originalFinalDateDiv1  = row.ORIGINAL_FINAL_DATE_DIV1
			a.country				= row.country

			retList.add(a)
		}
		println "size= "+retList.size()
		return retList
	}


	public List getSalesForceECDeletes() {
		List retList = new ArrayList()

		String deletesView = jr.useProdDBTables ?
					"SALES_FORCE_EC_DELETES" :
					"SALES_FORCE_EC_DELETES_TEST"

		String sqlString = 
			"""
                SELECT NCS_FOLDER, SPORT_CODE FROM ${deletesView}
            """
		sql.eachRow(sqlString)
		{ row ->

			EcPsaExport a = new EcPsaExport()
			a.ncsFolder     = row.ncs_folder
			a.sportCode     = row.sport_code

			retList.add(a)
		}

		return retList
	}


	public void createDBSnapshot() {
		if(jr.useProdDBTables)
			sql.call(" call sales_force_manager.createSnapshot()")
		else
			sql.call(" call sales_force_manager.createSnapshot_test()")
	}


	public int getMaxRownum() {
		int maxRownum
		String snapshotTable =  jr.useProdDBTables ?
								this.SALES_FORCE_EC : 
								this.SALES_FORCE_EC_TEST

		String sqlString = 
			"""
			SELECT MAX(ROW_NUM) the_max
            FROM ${snapshotTable}
		     WHERE SNAPSHOT_VERSION =
              (SELECT MAX (SNAPSHOT_VERSION) FROM ${snapshotTable})
            """
		sql.eachRow( sqlString )
		{ row ->
			maxRownum = row.the_max
		}

		return maxRownum
	}

	/**
	 * chunkSize=25000
	 * chunkLimit = 100 for prod and 1 for testing
	 * This limits the test snapshot to 25,000 records.
	 */

	public String runChunker(ExportFileCreator efc, int chunkSize, int chunkLimit){
		String snapFileName
		int countTotal = this.getMaxRownum()
		println "countTotal = "+countTotal
		int counter = 0

		while (counter <= countTotal && chunkLimit > 0 ) {
			List chunk = chunkSnapshot(counter, (counter+chunkSize-1) )
			String snapFileNameTemp = efc.appendSnapshot(chunk)
			if(snapFileNameTemp != null)
				snapFileName = snapFileNameTemp

			counter += chunkSize
			if(chunkLimit >= 0) chunkLimit --
		}
		return snapFileName
	}

	public List chunkSnapshot(int beginRownum, int endRownum) {
		println "from " +beginRownum+ " to "+endRownum
		List retList = new ArrayList()

		String snapshotTable =  jr.useProdDBTables ?
								this.SALES_FORCE_EC : 
								this.SALES_FORCE_EC_TEST

		String sqlString = 
		"""
		SELECT
            NCS_FOLDER,
  		    FIRST_NAME,
  		    LAST_NAME,
            SRF_DATE,
            EMAIL,
            AM_ENROLL_ID,
            ENROLL_SEMESTER,
            ENROLL_YEAR,
            OPEN_101_FORM,
            INTL_FLAG,
            home_school_staff,
            split_flag,
            cdn_flag,
            have_test_score,
            graduation_date,
            ready_to_process,
            min_task_date,
            max_task_date,
            sport_code,
            request_final_date,
            div1_decision_date,
            div1_sport_decision_id,
            div2_decision_date,
            div2_sport_decision_id,
            internal_sport_decision_id,
            recruit_cycle,
            cert_type_div1,
            cert_type_div2,
            pe_div1,
            pe_div2,
            payment_status,
			added_date,
			account_type,
			file_stat,
			billp_div1, billf_div1,
			country
        FROM
            ${snapshotTable}
            where snapshot_version = ( select max(snapshot_version) from ${snapshotTable} )
			and row_num between(${beginRownum}) and (${endRownum})
		"""
		println "sqlString=" +sqlString
		sql.eachRow(sqlString)
		{ row ->
			EcPsaExport a 			= new EcPsaExport()
			a.ncsFolder     		= row.ncs_folder
			a.firstName				= row.first_name
			a.lastName				= row.last_name
			a.srfDate               = row.srf_date
			a.email                 = row.email
			a.amEnrollId    		= row.am_enroll_id
			a.enrollSemester		= row.enroll_semester
			a.enrollYear    		= row.enroll_year
			a.open101Form   		= row.open_101_form
			a.intlFlag              = row.intl_flag
			a.homeSchoolStaff 		= row.home_school_staff
			a.splitFlag             = row.split_flag
			a.cdnflag               = row.cdn_flag
			a.haveTestScore 		= row.have_test_score
			a.graduationDate 		= row.graduation_date
			a.readyToProcess 		= row.ready_to_process
			a.minTaskDate   		= row.min_task_date
			a.maxTaskDate   		= row.max_task_date
			a.sportCode             = row.sport_code
			a.requestFinalDate 		= row.request_final_date
			a.div1DecisionDate 		= row.div1_decision_date
			a.div1SportDecisionId 	= row.div1_sport_decision_id
			a.div2DecisionDate 		= row.div2_decision_date
			a.div2SportDecisionId 	= row.div2_sport_decision_id
			a.internalSportDecisionId = row.internal_sport_decision_id
			a.recruitCycle  		= row.recruit_cycle
			a.certTypeDiv1 			= row.cert_type_div1
			a.certTypeDiv2 			= row.cert_type_div2
			a.peDiv1        		= row.pe_div1
			a.peDiv2        		= row.pe_div2
			a.paymentStatus 		= row.payment_status
			a.addedDate				= row.added_date
			a.accountType 			= row.account_type
			a.fileStat			= row.file_stat
			a.originalPrelimDateDiv1 = row.billp_div1  //in view:  ORIGINAL_PRELIM_DATE_DIV1
			a.originalFinalDateDiv1  = row.billf_div1  //in view: ORIGINAL_FINAL_DATE_DIV1
			a.country				= row.country

			retList.add(a)
		}
		println "size= "+retList.size()
		return retList
	}


	public static void main(String[] args){
		DBManager dbc = new DBManager();
		// println "test connection, studentCount = "+ dbc.getAnnouncementCount()
		// dbc.getEmailList()
		dbc.chunkSnapshot();
		println "done"
	}
}
