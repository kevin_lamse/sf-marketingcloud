package org.ncaa.sfmc.ecpsa;

import java.text.SimpleDateFormat;
import org.ncaa.sfmc.ecpsa.entities.EcPsaExport;
import org.ncaa.sfmc.ecpsa.JobRunner;


class ExportFileCreator {

	final String EXPORT_DIR = "EXPORT"
	final String EC_MODS_FILE_PREFIX = "EC_MODS"
	final String EC_SNAPSHOT_FILE_PREFIX = "EC_SNAPSHOT"
	final String EC_DELETES_FILE_PREFIX = "EC_DELETES"	
	final String SUFFIX = ".csv"
	final String SUFFIX_TXT = ".txt"

	File snapshotFile =  null
	JobRunnerConfig jr

	public ExportFileCreator(JobRunnerConfig jr) {
		this.jr = jr
	}

	public String createECModsCSV(List list) {
		println "createEcModsCSV()"

		String fileName
		if(jr.useProdDBTables) 	fileName = EXPORT_DIR+"/"+EC_MODS_FILE_PREFIX+"-"+jr.dateFormatted+SUFFIX
		else		 	fileName = EXPORT_DIR+"_TEST/"+EC_MODS_FILE_PREFIX+"-"+jr.dateFormatted+SUFFIX

		File f = new File(fileName)

		f.append("NCS_FOLDER,FIRST_NAME,LAST_NAME,SRF_DATE,EMAIL,")
		f.append("AM_ENROLL_ID,ENROLL_SEMESTER,ENROLL_YEAR,")
		f.append("OPEN_101_FORM,INTL_FLAG,HOME_SCHOOL_STAFF,")
		f.append("SPLIT_FLAG,CDN_FLAG,HAVE_TEST_SCORE,")
		f.append("GRADUATION_DATE,READY_TO_PROCESS,MIN_TASK_DATE,MAX_TASK_DATE,SPORT_CODE,")
		f.append("REQUEST_FINAL_DATE,DIV1_DECISION_DATE,DIV1_SPORT_DECISION_ID,")
		f.append("DIV2_DECISION_DATE,DIV2_SPORT_DECISION_ID,INTERNAL_SPORT_DECISION_ID,")
		f.append("RECRUIT_CYCLE,CERT_TYPE_DIV1,CERT_TYPE_DIV2,PE_DIV1,PE_DIV2,PAYMENT_STATUS,")
		f.append("ADDED_DATE,ACCOUNT_TYPE,FILE_STAT,ORIGINAL_PRELIM_DATE_DIV1,ORIGINAL_FINAL_DATE_DIV1,COUNTRY\n")

		for(EcPsaExport a : list) {
			f.append(rv(a.ncsFolder)+","+rv(a.firstName)+","+rv(a.lastName)+","+rvf(a.srfDate)+","+rv(a.email)+",")
			f.append(rv(a.amEnrollId)+","+rv(a.enrollSemester)+","+rv(a.enrollYear)+",")
			f.append(rvf(a.open101Form)+","+rv(a.intlFlag)+","+rv(a.homeSchoolStaff)+",")
			f.append(rv(a.splitFlag)+","+rv(a.cdnflag)+","+rv(a.haveTestScore)+",")
			f.append(rvf(a.graduationDate)+","+rv(a.readyToProcess)+","+rvf(a.minTaskDate)+","+rvf(a.maxTaskDate)+","+rv(a.sportCode)+",")
			f.append(rvf(a.requestFinalDate)+","+rvf(a.div1DecisionDate)+","+rv(a.div1SportDecisionId)+",")
			f.append(rvf(a.div2DecisionDate)+","+rv(a.div2SportDecisionId)+","+rv(a.internalSportDecisionId)+",")
			f.append(rv(a.recruitCycle)+","+rv(a.certTypeDiv1)+","+rv(a.certTypeDiv2)+","+rv(a.peDiv1)+","+rv(a.peDiv2)+","+rv(a.paymentStatus)+",")
			f.append(rvf(a.addedDate)+","+rv(a.accountType)+","+rv(a.fileStat)+","+rvf(a.originalPrelimDateDiv1)+","+rvf(a.originalFinalDateDiv1)+","+rv(a.country)+ "\n")

		}

		return fileName
	}


	public String appendSnapshot(List list) {
		println "appendSnapshot()"
		String snapshot
		boolean useHeader = false
		if (snapshotFile == null) {
			useHeader = true
			snapshot = EXPORT_DIR+"_TEST/"+EC_SNAPSHOT_FILE_PREFIX+"-"+jr.dateFormatted+SUFFIX
			if(jr.useProdDBTables) 	snapshot = EXPORT_DIR+"/"+EC_SNAPSHOT_FILE_PREFIX+"-"+jr.dateFormatted+SUFFIX
			snapshotFile = new File(snapshot)
		}
		
		if(useHeader) {
			snapshotFile.append("NCS_FOLDER,FIRST_NAME,LAST_NAME,SRF_DATE,EMAIL,")
			snapshotFile.append("AM_ENROLL_ID,ENROLL_SEMESTER,ENROLL_YEAR,")
			snapshotFile.append("OPEN_101_FORM,INTL_FLAG,HOME_SCHOOL_STAFF,")
			snapshotFile.append("SPLIT_FLAG,CDN_FLAG,HAVE_TEST_SCORE,")
			snapshotFile.append("GRADUATION_DATE,READY_TO_PROCESS,MIN_TASK_DATE,MAX_TASK_DATE,SPORT_CODE,")
			snapshotFile.append("REQUEST_FINAL_DATE,DIV1_DECISION_DATE,DIV1_SPORT_DECISION_ID,")
			snapshotFile.append("DIV2_DECISION_DATE,DIV2_SPORT_DECISION_ID,INTERNAL_SPORT_DECISION_ID,")
			snapshotFile.append("RECRUIT_CYCLE,CERT_TYPE_DIV1,CERT_TYPE_DIV2,PE_DIV1,PE_DIV2,PAYMENT_STATUS,")
			snapshotFile.append("ADDED_DATE,ACCOUNT_TYPE,FILE_STAT,ORIGINAL_PRELIM_DATE_DIV1,ORIGINAL_FINAL_DATE_DIV1,COUNTRY\n")
			
		}

		for(EcPsaExport a : list) {
			snapshotFile.append(rv(a.ncsFolder)+","+rv(a.firstName)+","+rv(a.lastName)+","+rvf(a.srfDate)+","+rv(a.email)+",")
			snapshotFile.append(rv(a.amEnrollId)+","+rv(a.enrollSemester)+","+rv(a.enrollYear)+",")
			snapshotFile.append(rvf(a.open101Form)+","+rv(a.intlFlag)+","+rv(a.homeSchoolStaff)+",")
			snapshotFile.append(rv(a.splitFlag)+","+rv(a.cdnflag)+","+rv(a.haveTestScore)+",")
			snapshotFile.append(rvf(a.graduationDate)+","+rv(a.readyToProcess)+","+rvf(a.minTaskDate)+","+rvf(a.maxTaskDate)+","+rv(a.sportCode)+",")
			snapshotFile.append(rvf(a.requestFinalDate)+","+rvf(a.div1DecisionDate)+","+rv(a.div1SportDecisionId)+",")
			snapshotFile.append(rvf(a.div2DecisionDate)+","+rv(a.div2SportDecisionId)+","+rv(a.internalSportDecisionId)+",")
			snapshotFile.append(rv(a.recruitCycle)+","+rv(a.certTypeDiv1)+","+rv(a.certTypeDiv2)+","+rv(a.peDiv1)+","+rv(a.peDiv2)+","+rv(a.paymentStatus)+",")
			snapshotFile.append(rvf(a.addedDate)+","+rv(a.accountType)+","+rv(a.fileStat)+","+rvf(a.originalPrelimDateDiv1)+","+rvf(a.originalFinalDateDiv1)+","+rv(a.country)+ "\n")
		}
		return snapshot
	}

	public String createECDeletesCSV(List list) {
		println "createEcModsCSV()"
		String fileName 
		if(jr.useProdDBTables) 	fileName = EXPORT_DIR+"/"+EC_DELETES_FILE_PREFIX+"-"+jr.dateFormatted+SUFFIX
		else		 	fileName = EXPORT_DIR+"_TEST/"+EC_DELETES_FILE_PREFIX+"-"+jr.dateFormatted+SUFFIX
		
		File f = new File(fileName)
		
		f.append("NCS_FOLDER,SPORT_CODE\n")
		
		for(EcPsaExport a : list) {
			f.append(rv(a.ncsFolder)+","+rv(a.sportCode)+"\n")
		}

		if(list.empty) {
			f.append( "\"1\",\"XYZ\"\n")	
		}
		
		return fileName
	}


	private String rvf(java.sql.Timestamp d) {
		String rval = ""

		if(d != null)
			rval = "\""+d.format("yyyy-MM-dd")+"\""
		else
			rval = "2000-01-01" //default for null.
		return rval
	}
	private String rv(java.sql.Timestamp d) {
		String rval = ""

		if(d != null)
		rval = "\""+d+"\""
		return rval
	}

	private String rv(String s) {
		String rval = ""

		if(s != null)
		rval = "\""+ s +"\""
		return rval
	}

	private String rv(int s) {
		String rval = ""

		if(s != null)
		rval = "\""+ s +"\""
		return rval
	}

	private String rv2(String s) {
		String rval = ""

		if(s != null)
		rval = s
		return rval
	}

	private String stripit(String s) {
		String rval = ""

		if(s != null) {
			rval = s.replaceAll("\\n","")
			rval = rval.replaceAll("\\r","")
			rval = rval.replaceAll("\\t","")
		}
		return rval
	}
}
