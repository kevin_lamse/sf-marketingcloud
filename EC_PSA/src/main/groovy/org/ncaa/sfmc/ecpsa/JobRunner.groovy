package org.ncaa.sfmc.ecpsa;

import org.ncaa.sfmc.ecpsa.entities.*;
import org.ncaa.sfmc.ecpsa.DBManager;
import org.ncaa.sfmc.ecpsa.ExportFileCreator;
import org.ncaa.sfmc.ecpsa.JobRunner;
import org.ncaa.sfmc.ecpsa.JobRunnerConfig;
import org.ncaa.sfmc.ecpsa.Sftp;

import groovy.util.logging.*;

/**
 * Create announcement csv file for LSDBi accouncements.
 * Create MEMBER and MEMBER_SPORT files for SalesForce DB of membership tables.
 * 
 * 
 * @author klamse
 *
 */
@Log4j2
class JobRunner {


	//Use composition, not inheritence
	JobRunnerConfig jrc 

	public JobRunner() {
		this.jrc = new JobRunnerConfig()
	}

	public String createECModsFile(DBManager dbm, ExportFileCreator efc) {
		List l = dbm.getSalesForceECMods()

		return efc.createECModsCSV(l)
	}

	public String createECDeletesFile(DBManager dbm, ExportFileCreator efc) {
		List l = dbm.getSalesForceECDeletes()

		return efc.createECDeletesCSV(l)
	}


	public static void main(String[] args) {
		Date startD = new Date()

		JobRunner jr = new JobRunner()
/*		
		println "JobRunner.main() for "+jr.projectName
		println "(JobRunner)this.createDBSnapshot= " +jr.createDBSnapshot
		println "(JobRunner)this.createModsFile= " +jr.createModsFile
		println "(JobRunner)this.createDeletesFile= " +jr.createDeletesFile
		println "(JobRunner)this.sendFiles= " +jr.sendFiles
		println "(JobRunner)this.showDebug= " +jr.showDebug
*/			
		jr.jrc.logfile.append("START: =============================================  \n")
		jr.jrc.logfile.append("START: "+startD.format("yyyyMMdd hh:mm:ss a") +"\n")
		jr.jrc.logfile.append("START: =============================================  \n")
		jr.jrc.logfile.append(" -createDBSnapshot = "+jr.jrc.createDBSnapshot+ "  \n")
		jr.jrc.logfile.append(" -useProdDB = "+jr.jrc.useProdDB+ "  \n")
		jr.jrc.logfile.append(" -useProdDBTables = "+jr.jrc.useProdDBTables+"  \n")

		ExportFileCreator efc = new ExportFileCreator(jr.jrc)
		DBManager dbm = new DBManager(jr.jrc)
		Sftp sftp = new Sftp(jr.jrc)

		if(jr.jrc.createDBSnapshot) {
			jr.jrc.logfile.append("CREATE: Snapshot\n")
			dbm.createDBSnapshot()
		} else {
			jr.jrc.logfile.append("SKIP: CREATING SNAPSHOT IN DB TABLE\n")
		}
		//Mods
		if(jr.jrc.createModsFile) {
			String ecModsFile = jr.createECModsFile(dbm,efc)
			jr.jrc.logfile.append "CREATE: MODS File= "+ecModsFile +"\n"
			if(ecModsFile != null && jr.jrc.sendFiles) {
				sftp.sendFile(ecModsFile)
			}
		} else {
			jr.jrc.logfile.append "SKIP: MODS File\n"
		}
		//Deletes
		if(jr.jrc.createDeletesFile) {
			String ecDeletesFile = jr.createECDeletesFile(dbm,efc)
			jr.jrc.logfile.append "CREATE: DELETES Files= "+ecDeletesFile +"\n"
			if(ecDeletesFile != null && jr.jrc.sendFiles) {
				sftp.sendFile(ecDeletesFile)
			}
		} else {
			jr.jrc.logfile.append "SKIP: DELETES\n"
		}
		
		//Send Trigger File: An automation is kicked off each time this file is sent. 
		if(jr.jrc.sendTriggerFile) {
			sftp.sendTriggerFile()
			jr.jrc.logfile.append "DO: SEND TRIGGER FILE\n"
			
		} else {
			jr.jrc.logfile.append "SKIP: SENT TRIGGER FILE\n"
		}
		Date endD = new Date()
		jr.jrc.logfile.append("END: "+endD.format("yyyyMMdd hh:mm:ss a") +"\n")
		jr.jrc.logfile.append("======================================")
	}
}
