package org.ncaa.sfmc.ecpsa;

import java.io.File;

import groovy.util.logging.*;

@Log4j2
class JobRunnerConfig {

	final String LOG_DIR = "logs/"
	String LOG_FILE = "EC_PSA"
	protected File logfile
	//File log
	String dateFormatted
	String dateFormattedLong
	
	boolean createDBSnapshot = false
	boolean createModsFile = false
	boolean createDeletesFile = false
	boolean sendFiles = true
	boolean sendTriggerFile = true
	boolean showDebug = true
    boolean useProdDB = true
    boolean useProdDBTables = false
	int		snapshotChunkSize = 0
	int		snapshotChunkLimit = 0

	String projectName = "SalesForce Cloud Marketing"
	String toDir
	String toDirTest
	String toDirTrigger
	String toDirTriggerTest
	String password
	AntBuilder ant


	String dbConnection
	String dbUsername
	String dbPassword

	String dbConnection_test
	String dbUsername_test
	String dbPassword_test

	public JobRunnerConfig() {
		setProperties()
		createLogfile()
		ant = new AntBuilder() //do I need so many?
		logfile.append "\nJobRunnerConfig() initialized \n"
	}

	private void createLogfile() {
		Date d = new Date()
		dateFormatted = d.format("yyyy-MM-dd")
		dateFormattedLong = d.format("yyyy-MM-dd-Hms")
		log.info "dateFormatted= " + dateFormatted
		logfile = new File(this.LOG_DIR+this.LOG_FILE+"-"+dateFormatted+".log")
		log.info ("(log.info)start at {} ",dateFormatted) // does this work?

	}

	private void setProperties() {
		String propFile = 'src/main/resources/job.properties'
		log.info "JobRunnerConfig.setProperties() from " +propFile
		Properties properties = new Properties()
		File propertiesFile = new File(propFile)
		propertiesFile.withInputStream { properties.load(it) }

        if(properties."sfmc.useProdDB")
            this.useProdDB = properties."sfmc.useProdDB".toBoolean()
        log.info "this.useProdDB = "+this.useProdDB

        if(properties."sfmc.useProdDBTables")
            this.useProdDBTables = properties."sfmc.useProdDBTables".toBoolean()
        log.info "this.useProdDBTables = "+this.useProdDBTables


		if(properties."sfmc.project.name")
			this.projectName = properties."sfmc.project.name"
		log.info "this.projectName = "+this.projectName

		if(properties."sfmc.create.DBSnapshot")
			this.createDBSnapshot = properties."sfmc.create.DBSnapshot".toBoolean()
		log.info "this.createDBSnapshot = "+this.createDBSnapshot

		if(properties."sfmc.create.MODS.File")
			this.createModsFile = properties."sfmc.create.MODS.File".toBoolean()
		log.info "this.createModsFile = "+this.createModsFile

		if(properties."sfmc.create.DELETES.File")
			this.createDeletesFile = properties."sfmc.create.DELETES.File".toBoolean()
		log.info "this.createDeletesFile = "+this.createDeletesFile

		if(properties."sfmc.sendFiles")
			this.sendFiles = properties."sfmc.sendFiles".toBoolean()
		log.info "this.sendFiles = "+this.sendFiles

		if(properties."sfmc.sendTriggerFile")
			this.sendFiles = properties."sfmc.sendTriggerFile".toBoolean()
		log.info "this.sendTriggerFile = "+this.sendTriggerFile

		if(properties."sfmc.showDebug")
			this.showDebug = properties."sfmc.showDebug".toBoolean()
		log.info "this.showDebug = "+this.showDebug

		if(properties."sfmc.sftp.url")
			this.toDir = properties."sfmc.sftp.url"
		log.info "this.toDir (ftp URL)  = "+this.toDir

		if(properties."sfmc.test.sftp.url")
			this.toDirTest = properties."sfmc.test.sftp.url"
		log.info "this.toDirTest (ftp URL) = "+this.toDirTest

		if(properties."sfmc.trigger.sftp.url")
			this.toDirTrigger = properties."sfmc.trigger.sftp.url"
			log.info "this.toDirTrigger = "+this.toDirTrigger

		if(properties."sfmc.test.trigger.sftp.url")
			this.toDirTriggerTest = properties."sfmc.test.trigger.sftp.url"
			log.info "this.toDirTriggerTest = "+this.toDirTriggerTest

		if(properties."sfmc.sftp.password")
			this.password= properties."sfmc.sftp.password"
		//log.info "this.password = "+this.password

		// How large is the resultset?
		// sfmc.snapshot.chunk.size=25000 is default
		if(properties."sfmc.snapshot.chunk.size")
			this.snapshotChunkSize = properties."sfmc.snapshot.chunk.size" as Integer
		log.info "this.snapshotChunkSize = "+this.snapshotChunkSize

		// How many chunks will be exported? set to 1 for testing or 100 to insure full set
		if(properties."sfmc.snapshot.chunk.limit")
			this.snapshotChunkLimit = properties."sfmc.snapshot.chunk.limit" as Integer
		log.info "this.snapshotChunkLimit = "+this.snapshotChunkLimit


		//Load another file
		propFile = 'src/main/resources/jdbc.properties'
		log.info "JobRunnerConfig.setProperties() from " +propFile
		propertiesFile = new File(propFile)
		propertiesFile.withInputStream { properties.load(it) }
		if(properties."ncaa.dbConnection")
			this.dbConnection = properties."ncaa.dbConnection"
		log.info "this.dbConnection = "+this.dbConnection

		if(properties."ncaa.dbUsername")
			this.dbUsername = properties."ncaa.dbUsername"
		log.info "this.dbUsername = "+this.dbUsername

		if(properties."ncaa.dbPassword")
			this.dbPassword = properties."ncaa.dbPassword"

		if(properties."test.ncaa.dbConnection")
			this.dbConnection_test = properties."test.ncaa.dbConnection"
		log.info "this.dbConnection_test = "+this.dbConnection_test

		if(properties."test.ncaa.dbUsername")
			this.dbUsername_test = properties."test.ncaa.dbUsername"
		log.info "this.dbUsername_test = "+this.dbUsername_test

		if(properties."test.ncaa.dbPassword")
			this.dbPassword_test = properties."test.ncaa.dbPassword"

/*
		//Env not using. 
		def env = System.getenv()

		if(env && env['CRONJOB_BASEDIR']) {
		        this.baseDir = env['ACT_UPLOAD_BASEDIR']
		}
		log.info "this.baseDir = "+this.baseDir
*/
	}

}
