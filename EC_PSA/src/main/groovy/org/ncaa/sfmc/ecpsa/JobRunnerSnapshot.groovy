package org.ncaa.sfmc.ecpsa;

import org.ncaa.sfmc.ecpsa.entities.*;
import org.ncaa.sfmc.ecpsa.DBManager;
import org.ncaa.sfmc.ecpsa.ExportFileCreator;
import org.ncaa.sfmc.ecpsa.JobRunner;
import org.ncaa.sfmc.ecpsa.JobRunnerConfig;
import org.ncaa.sfmc.ecpsa.Sftp;

import groovy.util.logging.*;

/**
 * TODO: Add docs
 * 
 * 
 * @author klamse
 *
 */
@Log4j2
class JobRunnerSnapshot extends JobRunner {


	public JobRunnerSnapshot() {

	}

	public static void main(String[] args) {
		Date startD = new Date()

		JobRunnerSnapshot jr = new JobRunnerSnapshot()
		log.info "Starting JobRunnerSnapshot: "

		jr.jrc.logfile.append("START: =============================================  \n")
		jr.jrc.logfile.append("START: "+startD.format("yyyyMMdd hh:mm:ss a") +"\n")
		jr.jrc.logfile.append("START: =============================================  \n")

		ExportFileCreator efc = new ExportFileCreator(jr.jrc)
		DBManager dbm = new DBManager(jr.jrc)
		Sftp sftp = new Sftp(jr.jrc)

		if(jr.jrc.createDBSnapshot) {
			jr.jrc.logfile.append("CREATE: Snapshot\n")
			dbm.createDBSnapshot()
		} else {
			jr.jrc.logfile.append("SKIP: CREATING SNAPSHOT IN DB TABLE\n")
		}

		log.info "createSnapshotFile now!"

		String ecSnapshotFile = dbm.runChunker(efc, jr.jrc.snapshotChunkSize, jr.jrc.snapshotChunkLimit)
		jr.jrc.logfile.append "CREATE: SNAPSHOT FILE= "+ecSnapshotFile +",snapshotChunkSize= "+jr.jrc.snapshotChunkSize+",snapshotChunkLimit= "+jr.jrc.snapshotChunkLimit+"\n"
		if(ecSnapshotFile != null && jr.jrc.sendFiles ) {
			sftp.sendFile(ecSnapshotFile)
		}
		Date endD = new Date()
		jr.jrc.logfile.append("END: "+endD.format("yyyyMMdd hh:mm:ss a") +"\n")
		jr.jrc.logfile.append("======================================")
	}
}
