package org.ncaa.sfmc.ecpsa;

import org.ncaa.sfmc.ecpsa.JobRunner;

class Sftp {

	final String TRIGGER_FILE_SUFFIX = "EC_READY"
	final String TRIGGER_DIR = "TRIGGER"
	
	JobRunnerConfig jr
	String toDir
	String password

	public Sftp(JobRunnerConfig jr) {
		println "Sftp() initialized"
		this.jr = jr
	}

	public void sendFile(String fileToSend ) {
		jr.logfile.append " ->Sftp.sendFile "+ fileToSend
		String toDirFinal
		if(jr.useProdDBTables) toDirFinal = jr.toDir
		else toDirFinal = jr.toDirTest
		println " ->sending to: "+toDirFinal
		jr.ant.scp(
				file: fileToSend,
				todir: toDirFinal,
				password: jr.password,
				sftp: true,
				trust: true,
				verbose: true
				)
	}
	
	//Hard code values for now.  Assume it doesn't change..
	public void sendTriggerFile() {
		String triggerFileName = TRIGGER_DIR+"/"+TRIGGER_FILE_SUFFIX+"-"+jr.dateFormattedLong+".txt"
		println "triggerFileName =" +triggerFileName
		File triggerFile = new File(triggerFileName)
		triggerFile.write("This is my trigger")
		String toDirFinal
		if(jr.useProdDBTables) toDirFinal = jr.toDirTrigger
		else toDirFinal = jr.toDirTriggerTest
		println " ->sending to: "+toDirFinal
		jr.ant.scp(
				file: triggerFileName,
				todir: toDirFinal,
				password: jr.password,
				sftp: true,
				trust: true,
				verbose: true
				)
	}
}
