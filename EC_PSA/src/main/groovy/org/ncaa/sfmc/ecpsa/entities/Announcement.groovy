package org.ncaa.sfmc.ecpsa.entities;

import groovy.transform.ToString;

@ToString
public class Announcement {

	String email
	String emailText
}
