package org.ncaa.sfmc.ecpsa.entities

class EcPsaExport {

	String 		ncsFolder
	String		firstName
	String		lastName
	Date		srfDate
	String 		email
	String 		amEnrollId
	String		enrollSemester
	String		enrollYear
	Date		open101Form
	String		intlFlag
	String 		homeSchoolStaff
	String		splitFlag
	String 		cdnflag
	String 		haveTestScore
	Date		graduationDate
	String		readyToProcess
	Date		minTaskDate
	Date		maxTaskDate
	String		sportCode
	Date		requestFinalDate
	Date		div1DecisionDate
	String		div1SportDecisionId
	Date		div2DecisionDate
	String		div2SportDecisionId
	String		internalSportDecisionId
	String		recruitCycle
	String      certTypeDiv1
	String      certTypeDiv2
	String      peDiv1
	String      peDiv2
	String      paymentStatus
	Date		addedDate
	String		accountType
	String		fileStat
	Date		originalPrelimDateDiv1
	Date		originalFinalDateDiv1
	String		country
		
}
