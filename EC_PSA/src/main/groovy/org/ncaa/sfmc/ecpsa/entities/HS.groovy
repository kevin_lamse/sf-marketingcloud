package org.ncaa.sfmc.ecpsa.entities;

import groovy.transform.ToString;

@ToString
public class HS {
	
	String hscode
	String name
	String pin
	Date regstrnInfoSentDate
	Date minTaskDate
	Date maxTaskDate
	String email
}
