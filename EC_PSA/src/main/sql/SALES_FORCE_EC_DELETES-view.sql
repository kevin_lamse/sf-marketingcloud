CREATE OR REPLACE FORCE VIEW "NCAA"."SALES_FORCE_EC_DELETES" ("NCS_FOLDER", "SPORT_CODE") AS 
  (
  select ncs_folder, sport_code
  from SALES_FORCE_EC where SNAPSHOT_VERSION = (select max(SNAPSHOT_VERSION)-1 from SALES_FORCE_EC)
  minus
  select ncs_folder, sport_code
  from SALES_FORCE_EC where SNAPSHOT_VERSION = (select max(SNAPSHOT_VERSION) from SALES_FORCE_EC) 
);