CREATE OR REPLACE FORCE VIEW "NCAA"."SALES_FORCE_EC_DELETES_TEST" ("NCS_FOLDER", "SPORT_CODE") AS 
  (
  select ncs_folder, sport_code
  from SALES_FORCE_EC_TEST where SNAPSHOT_VERSION = (select max(SNAPSHOT_VERSION)-1 from SALES_FORCE_EC_TEST)
  minus
  select ncs_folder, sport_code
  from SALES_FORCE_EC_TEST where SNAPSHOT_VERSION = (select max(SNAPSHOT_VERSION) from SALES_FORCE_EC_TEST) 
);