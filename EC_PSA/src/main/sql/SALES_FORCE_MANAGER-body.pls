create or replace PACKAGE BODY      Sales_Force_Manager
AS
   /******************************************************************************
      NAME:       SalesForce
      PURPOSE:

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        10/13/2015      klamse       1. Created this package body.


      Note: ncs_folder, sport_code are the keys for SALES_FORCE_EC table.
      This affects the views and the process.  If this changes, the process
      should be re-evaluated.

      VIEWS:
      sales_force_ec_mods:  View shows the changes since the last snapshot.
        CURRENT_SNAPSHOT - PREVIOUS_SNAPSHOT
      sales_forcs_ec_deletes:  View shows which ncs_folder, sport_code records have been removed.
        **If key (ncs_folder, sport_code) changes then RE-EVAL THIS PROCESS!**
      Each of these views will be sent to SalesForce so that can update their system.
      The initial record set is sent as a csv file, then daily updates are copied to their
      sftp site.
      The amount of time between the snapshots does not matter.
      The views look at only the 2 most current snapshots. History will not be saved.

   ******************************************************************************/


   PROCEDURE createSnapshot
   IS
      v_snapshot_version   NUMBER := -1;

                   
   BEGIN
      SELECT NVL (MAX (snapshot_version), 0)
        INTO v_snapshot_version
        FROM SALES_FORCE_EC;

      v_snapshot_version := v_snapshot_version + 1;

      DBMS_OUTPUT.put_line ('v_snapshot_version= ' || v_snapshot_version);

      INSERT INTO SALES_FORCE_EC (ROW_NUM,
                                  NCS_FOLDER,
                                  FIRST_NAME,
                                  LAST_NAME,
                                  SRF_DATE,
                                  EMAIL,
                                  AM_ENROLL_ID,
                                  ENROLL_SEMESTER,
                                  ENROLL_YEAR,
                                  OPEN_101_FORM,
                                  INTL_FLAG,
                                  home_school_staff,
                                  split_flag,
                                  cdn_flag,
                                  have_test_score,
                                  graduation_date,
                                  ready_to_process,
                                  min_task_date,
                                  max_task_date,
                                  sport_code,
                                  request_final_date,
                                  div1_decision_date,
                                  div1_sport_decision_id,
                                  div2_decision_date,
                                  div2_sport_decision_id,
                                  internal_sport_decision_id,
                                  recruit_cycle,
                                  cert_type_div1, cert_type_div2, pe_div1, pe_div2,
                                  payment_status,
                                  added_date,
                                  account_type,
                                  file_stat,
                                  billp_div1,billf_div1)
            SELECT ROWNUM,
                innera.ncs_folder,
                innera.fname,
                innera.lname,
                innera.srf_date,
                innera.email,
                innera.am_enroll_id,
                innera.enroll_semester,
                innera.enroll_year,
                innera.open_101_form,
                innera.intl_flag,
                innera.home_school_staff,
                innera.split_flag,
                innera.cdn_flag,
                innera.have_test_score,
                innera.graduation_date,
                innera.ready_to_process,
                innera.min_task_date,
                innera.max_task_date,
                innerb.sport_code,
                innerb.request_final_date,
                innerb.div1_decision_date,
                innerb.div1_sport_decision_id,
                innerb.div2_decision_date,
                innerb.div2_sport_decision_id,
                innerb.internal_sport_decision_id,
                innerb.recruit_cycle,
                innera.cert_type_div1, innera.cert_type_div2, innera.pe_div1, innera.pe_div2,
                innera.payment_status,
                innera.added_date,
                innera.account_type,
                innera.file_stat,
                innera.billp_div1, innera.billf_div1
           FROM (SELECT s.ncs_folder,
                        s.fname,
                        s.lname,
                        s.srf_date,
                        s.email,
                        s.am_enroll_id,
                        ae.enroll_semester,
                        ae.enroll_year,
                        ae.open_101_form,
                        s.intl_flag,
                        s.home_school_staff,
                        s.split_flag,
                        s.cdn_flag,
                        s.test have_test_score,
                        s.graduation_date,
                        s.ready_to_process,
                        s.cert_type_div1, s.cert_type_div2, s.pe_div1, s.pe_div2,
                        t.min_task_date,
                        t.max_task_date,
                        s.payment_status,
                        s.added_date,
                        sa.account_type,
                        s.file_stat,
                        s.billp_div1, s.billf_div1
                   FROM student s
                        JOIN student_account sa on (s.ncs_folder = sa.ncs_folder)
                        JOIN am_enroll ae
                           ON (s.am_enroll_id = ae.am_enroll_id)
                        LEFT OUTER JOIN
                        (
                        select ncs_folder, min(date_task_assigned) min_task_date, max(date_task_assigned) max_task_date
                        from student_tasks where date_task_completed is null group by ncs_folder
                        ) t ON (t.ncs_folder = s.ncs_folder)
                  WHERE     active_ep_window (ae.am_enroll_id) = 'T'
                        AND s.email IS NOT NULL ) innera
                -- Sport Cert Data
                JOIN
                (SELECT sp.ncs_folder,
                        sp.sport_code,
                        sp.request_final_date,
                        sc.div1_decision_date,
                        sc.div1_sport_decision_id,
                        sc.div2_decision_date,
                        sc.div2_sport_decision_id,
                        sc.internal_sport_decision_id,
                        i.recruit_cycle
                   FROM sport_participation sp
                        JOIN sport_certification sc
                           ON (    sp.ncs_folder = sc.ncs_folder
                               AND sp.sport_code = sc.sport_code)
                        LEFT OUTER JOIN
                        (  SELECT DISTINCT
                                  ncs_folder,
                                  sport,
                                  MAX (recruit_cycle) recruit_cycle
                             FROM irl
                            WHERE active = 'Y'
                         GROUP BY ncs_folder, sport) i
                           ON (    sp.ncs_folder = i.ncs_folder
                               AND sp.sport_code = i.sport)) innerb
                   ON (innera.ncs_folder = innerb.ncs_folder);


      UPDATE SALES_FORCE_EC
         SET SNAPSHOT_VERSION = v_snapshot_version
       WHERE SNAPSHOT_VERSION IS NULL;

      delete from sales_force_ec where snapshot_version < v_snapshot_version -3; -- keep 3.
      logSnapshot(v_snapshot_version, 'PROD - END');

      COMMIT;
      
   END;

  /**
    2017-03-02: Add first_name and last_name
  */
   PROCEDURE createSnapshot_test
   IS
      v_snapshot_version   NUMBER := -1;

                   
   BEGIN
      SELECT NVL (MAX (snapshot_version), 0)
        INTO v_snapshot_version
        FROM SALES_FORCE_EC_TEST;

      v_snapshot_version := v_snapshot_version + 1;

      DBMS_OUTPUT.put_line ('TEST v_snapshot_version= ' || v_snapshot_version);

      INSERT INTO SALES_FORCE_EC_TEST (ROW_NUM,
                                  NCS_FOLDER,
                                  FIRST_NAME,
                                  LAST_NAME,
                                  SRF_DATE,
                                  EMAIL,
                                  AM_ENROLL_ID,
                                  ENROLL_SEMESTER,
                                  ENROLL_YEAR,
                                  OPEN_101_FORM,
                                  INTL_FLAG,
                                  home_school_staff,
                                  split_flag,
                                  cdn_flag,
                                  have_test_score,
                                  graduation_date,
                                  ready_to_process,
                                  min_task_date,
                                  max_task_date,
                                  sport_code,
                                  request_final_date,
                                  div1_decision_date,
                                  div1_sport_decision_id,
                                  div2_decision_date,
                                  div2_sport_decision_id,
                                  internal_sport_decision_id,
                                  recruit_cycle,
                                  cert_type_div1, cert_type_div2, pe_div1, pe_div2,
                                  payment_status,
                                  added_date,
                                  account_type,
                                  file_stat,
                                  billp_div1,billf_div1)
            SELECT ROWNUM,
                innera.ncs_folder,
                innera.fname,  
                innera.lname,
                innera.srf_date,
                innera.email,
                innera.am_enroll_id,
                innera.enroll_semester,
                innera.enroll_year,
                innera.open_101_form,
                innera.intl_flag,
                innera.home_school_staff,
                innera.split_flag,
                innera.cdn_flag,
                innera.have_test_score,
                innera.graduation_date,
                innera.ready_to_process,
                innera.min_task_date,
                innera.max_task_date,
                innerb.sport_code,
                innerb.request_final_date,
                innerb.div1_decision_date,
                innerb.div1_sport_decision_id,
                innerb.div2_decision_date,
                innerb.div2_sport_decision_id,
                innerb.internal_sport_decision_id,
                innerb.recruit_cycle,
                innera.cert_type_div1, innera.cert_type_div2, innera.pe_div1, innera.pe_div2,
                innera.payment_status,
                innera.added_date,
                innera.account_type,
                innera.file_stat,
                innera.billp_div1, innera.billf_div1
           FROM (SELECT s.ncs_folder,
                        s.fname,
                        s.lname,
                        s.srf_date,
                        s.email,
                        s.am_enroll_id,
                        ae.enroll_semester,
                        ae.enroll_year,
                        ae.open_101_form,
                        s.intl_flag,
                        s.home_school_staff,
                        s.split_flag,
                        s.cdn_flag,
                        s.test have_test_score,
                        s.graduation_date,
                        s.ready_to_process,
                        s.cert_type_div1, s.cert_type_div2, s.pe_div1, s.pe_div2,
                        t.min_task_date,
                        t.max_task_date,
                        s.payment_status,
                        s.added_date,
                        sa.account_type,
                        s.file_stat,
                        s.billp_div1, s.billf_div1
                   FROM student s
                        JOIN student_account sa on (s.ncs_folder = sa.ncs_folder)
                        JOIN am_enroll ae
                           ON (s.am_enroll_id = ae.am_enroll_id)
                        LEFT OUTER JOIN
                        (
                        select ncs_folder, min(date_task_assigned) min_task_date, max(date_task_assigned) max_task_date
                        from student_tasks where date_task_completed is null group by ncs_folder
                        ) t ON (t.ncs_folder = s.ncs_folder)
                  WHERE     active_ep_window (ae.am_enroll_id) = 'T'
                        AND s.email IS NOT NULL
                        AND sa.account_type = 'CERTIFICATION') innera
                -- Sport Cert Data
                JOIN
                (SELECT sp.ncs_folder,
                        sp.sport_code,
                        sp.request_final_date,
                        sc.div1_decision_date,
                        sc.div1_sport_decision_id,
                        sc.div2_decision_date,
                        sc.div2_sport_decision_id,
                        sc.internal_sport_decision_id,
                        i.recruit_cycle
                   FROM sport_participation sp
                        JOIN sport_certification sc
                           ON (    sp.ncs_folder = sc.ncs_folder
                               AND sp.sport_code = sc.sport_code)
                        LEFT OUTER JOIN
                        (  SELECT DISTINCT
                                  ncs_folder,
                                  sport,
                                  MAX (recruit_cycle) recruit_cycle
                             FROM irl
                            WHERE active = 'Y'
                         GROUP BY ncs_folder, sport) i
                           ON (    sp.ncs_folder = i.ncs_folder
                               AND sp.sport_code = i.sport)) innerb
                   ON (innera.ncs_folder = innerb.ncs_folder);


      UPDATE SALES_FORCE_EC_TEST
         SET SNAPSHOT_VERSION = v_snapshot_version
       WHERE SNAPSHOT_VERSION IS NULL;

      delete from sales_force_ec_test where snapshot_version < v_snapshot_version -2; -- keep 3.
      logSnapshot(v_snapshot_version, 'TEST - END');
      COMMIT;
      
   END;


   PROCEDURE logSnapshot (p_snapshot_id IN NUMBER, p_note IN VARCHAR2)
   IS
   
   BEGIN
     insert into sales_force_ec_snapshot_log(snapshot_version,note) values (p_snapshot_id,p_note);
   END;


   FUNCTION MyFunction (Param1 IN NUMBER)
      RETURN NUMBER
   IS
   BEGIN
      RETURN Param1;
   END;

  

   PROCEDURE MyProcedure (Param1 IN NUMBER)
   IS
      TmpVar   NUMBER;
   BEGIN
      TmpVar := Param1;
   END;
   
   
   
END Sales_Force_Manager;