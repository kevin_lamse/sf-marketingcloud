--scratch
select *  
FROM SALES_FORCE_EC
where ncs_folder = '1601405140'

--fix
select email from student where ncs_folder = '1304270929'

update student
set email = 'bill@kidsacrossamerica.org'
where ncs_folder = '1304270929'

--655272
--412 is current. 
select distinct snapshot_version from SALES_FORCE_EC

select * from SALES_FORCE_EC_test
select distinct snapshot_version from SALES_FORCE_EC_test

select * from sales_force_ec_snapshot_log

--============== PROD TABLES ===================
select *  
FROM SALES_FORCE_EC
     WHERE SNAPSHOT_VERSION =
              (SELECT MAX (SNAPSHOT_VERSION) FROM SALES_FORCE_EC)

select account_type, count(*) 
FROM SALES_FORCE_EC
     WHERE SNAPSHOT_VERSION =
              (SELECT MAX (SNAPSHOT_VERSION) FROM SALES_FORCE_EC)
  group by account_type

select trunc(added_date), account_type, count(*) 
from student_account where added_date > sysdate - 5
group by trunc(added_date), account_type
order by trunc(added_date), account_type

-- VIEWS
select * from sales_force_ec_mods;
select * from sales_force_ec_deletes;

select * from sales_force_ec_mods WHERE ACCOUNT_TYPE <> 'CERTIFICATION'

--2619938
select count(*)  
FROM SALES_FORCE_EC

--Create
exec sales_force_manager.createSnapshot()

delete from SALES_FORCE_EC where snapshot_version = 459

select snapshot_version, count(*) 
from SALES_FORCE_EC
group by snapshot_version
order by snapshot_version

-- Conclusion:  Don't need an outer join jon sport_certification for PROFILE accounts.  Decisions are of type 'I'. 
--debug adding PROFILE ACCOUNTS
select * from sport_certification where ncs_folder in (
select sp.ncs_folder from sport_participation sp join student_account sa on (sp.ncs_folder = sa.ncs_folder)
where sa.account_type <> 'CERTIFICATION'
)

select * from sport_participation sp 
join student_account sa on (sp.ncs_folder = sa.ncs_folder)
join sport_certification sc on (sp.ncs_folder = sc.ncs_folder and sp.sport_code = sc.sport_code)
where sa.account_type <> 'CERTIFICATION'

--============== TEST TABLES ===================
select *  
FROM SALES_FORCE_EC_TEST
     WHERE SNAPSHOT_VERSION =
              (SELECT MAX (SNAPSHOT_VERSION) FROM SALES_FORCE_EC_TEST);
select distinct snapshot_version from SALES_FORCE_EC_TEST;
select max(row_num)  
FROM SALES_FORCE_EC_TEST
     WHERE SNAPSHOT_VERSION =
              (SELECT MAX (SNAPSHOT_VERSION) FROM SALES_FORCE_EC_TEST);

select *  
FROM SALES_FORCE_EC_TEST
     WHERE SNAPSHOT_VERSION = 1
  --  and email like 'crogers2@ncaa.org'
minus 
select *  
FROM SALES_FORCE_EC_TEST
     WHERE SNAPSHOT_VERSION = 2
   -- and email like 'crogers2@ncaa.org'

--update student set fname = 'UNDER' where ncs_folder =  1305313291 and email = 'crogers2@ncaa.org'



-- VIEWS
select * from sales_force_ec_mods_test;
select * from sales_force_ec_deletes_test;

delete FROM SALES_FORCE_EC_TEST where SNAPSHOT_VERSION = 2

-- create additional snapshot Note this can take 20-30 minutes
exec sales_force_manager.createSnapshot_test()

select snapshot_version, count(*) 
from SALES_FORCE_EC_TEST
group by snapshot_version


-- count of account types
Select account_type, count(*) 
from student_account
group by account_type

-- TEST data
select * from sport_participation where ncs_folder = '1407748916'
update sport_participation set request_final_date = sysdate-1
where ncs_folder = '1407748916'


-- Added Date
-- Country Code
-- Proof of Graduation (date)
select s.added_date, s.country, s.received_pog, s.* from student s
-- Transcript: what are we looking for here? 
 -- corret?: T if count(*) > 0 for an PSA in this table, else it's F 
select trns, ncs_folder from hs_student where current_flag = 'Y' 
-- but found this in delivra code:
      SELECT COUNT ( * )
        INTO count_stud_cmnt
        FROM stud_cmnt sc
       WHERE sc.cmnt# IN (847, 851, 884) AND sc.ssn = p_ssn;

      SELECT COUNT ( * )
        INTO count_stud_elig
        FROM stud_elig se
       WHERE se.elig_code = 'H02' AND se.ssn = p_ssn;

      -- We are adding these numbers as whether final transcript has been received
      -- by EC or not is decided by one of these criteria. So if any one of the above
      -- two SQLs is returning any rows that means final transcript has been received.
      p_count_data := count_stud_cmnt + count_stud_elig;
      DBMS_OUTPUT.put_line ('count_data = ' || p_count_data);
------  Also found this...      
   SELECT COUNT ( * )
        INTO count_hs
        FROM hs_student
       WHERE ssn = p_ssn;

      SELECT ncs_folder
        INTO p_ncs_folder
        FROM student s
       WHERE s.ssn = p_ssn;

      IF count_hs = 0
      THEN
         p_text5 := 'Trans';
      ELSE
         SELECT COUNT ( * )
           INTO count_hs_stud
           FROM hs_student hs
          WHERE hs.TRNS IS NULL AND hs.ssn = p_ssn;

         IF count_hs_stud > 0
         THEN
            p_text5 := 'Trans';
         ELSE
            p_text5 := '';
         END IF;
      END IF;
---      


-- NLI: T if count(*) > 0 for an PSA in this table, else it's F
select * from nli_data_list where status_id = 12  --12 is valid


-- IRL  // active IRL of any school?  see irl.active
-- See delivra_ec_package
select * from irl

-- WR3 Account type (certification vs. profile)  // need to add values in student_account table. 




# main query for mods
                SELECT
                NCS_FOLDER,
                SRF_DATE,
                EMAIL,
                AM_ENROLL_ID,
                ENROLL_SEMESTER,
                ENROLL_YEAR,
                OPEN_101_FORM,
                INTL_FLAG,
                home_school_staff,
                split_flag,
                cdn_flag,
                have_test_score,
                graduation_date,
                ready_to_process,
                min_task_date,
                max_task_date,
                sport_code,
                request_final_date,
                div1_decision_date,
                div1_sport_decision_id,
                div2_decision_date,
                div2_sport_decision_id,
                internal_sport_decision_id,
                recruit_cycle,
                cert_type_div1,
                cert_type_div2,
                pe_div1,
                pe_div2,
                payment_status
        FROM
                sales_force_ec
                where snapshot_version = ( select max(snapshot_version) from sales_force_ec )

--main view
select * from sales_force_ec

#Deletes
SELECT NCS_FOLDER, SPORT_CODE FROM SALES_FORCE_EC_DELETES

# Stored Procedure:
call sales_force_manager.createSnapshot()


            SELECT innera.ncs_folder,
                innera.srf_date,
                innera.email,
                innera.am_enroll_id,
                innera.enroll_semester,
                innera.enroll_year,
                innera.open_101_form,
                innera.intl_flag,
                innera.home_school_staff,
                innera.split_flag,
                innera.cdn_flag,
                innera.have_test_score,
                innera.graduation_date,
                innera.ready_to_process,
                innera.min_task_date,
                innera.max_task_date,
                innerb.sport_code,
                innerb.request_final_date,
                innerb.div1_decision_date,
                innerb.div1_sport_decision_id,
                innerb.div2_decision_date,
                innerb.div2_sport_decision_id,
                innerb.internal_sport_decision_id,
                innerb.recruit_cycle,
                innera.cert_type_div1, innera.cert_type_div2, innera.pe_div1, innera.pe_div2,
                innera.payment_status
           FROM (SELECT s.ncs_folder,
                        s.srf_date,
                        s.email,
                        s.am_enroll_id,
                        ae.enroll_semester,
                        ae.enroll_year,
                        ae.open_101_form,
                        s.intl_flag,
                        s.home_school_staff,
                        s.split_flag,
                        s.cdn_flag,
                        s.test have_test_score,
                        s.graduation_date,
                        s.ready_to_process,
                        s.cert_type_div1, s.cert_type_div2, s.pe_div1, s.pe_div2,
                        t.min_task_date,
                        t.max_task_date,
                        s.payment_status
                   FROM student s
                        JOIN am_enroll ae
                           ON (s.am_enroll_id = ae.am_enroll_id)
                        LEFT OUTER JOIN
                        (
                        select ncs_folder, min(date_task_assigned) min_task_date, max(date_task_assigned) max_task_date
                        from student_tasks where date_task_completed is null group by ncs_folder
                        ) t ON (t.ncs_folder = s.ncs_folder)
                  WHERE     active_ep_window (ae.am_enroll_id) = 'T'
                        AND s.email IS NOT NULL) innera
                -- Sport Cert Data
                JOIN
                (SELECT sp.ncs_folder,
                        sp.sport_code,
                        sp.request_final_date,
                        sc.div1_decision_date,
                        sc.div1_sport_decision_id,
                        sc.div2_decision_date,
                        sc.div2_sport_decision_id,
                        sc.internal_sport_decision_id,
                        i.recruit_cycle
                   FROM sport_participation sp
                        JOIN sport_certification sc
                           ON (    sp.ncs_folder = sc.ncs_folder
                               AND sp.sport_code = sc.sport_code)
                        LEFT OUTER JOIN
                        (  SELECT DISTINCT
                                  ncs_folder,
                                  sport,
                                  MAX (recruit_cycle) recruit_cycle
                             FROM irl
                            WHERE active = 'Y'
                         GROUP BY ncs_folder, sport) i
                           ON (    sp.ncs_folder = i.ncs_folder
                               AND sp.sport_code = i.sport)) innerb
                   ON (innera.ncs_folder = innerb.ncs_folder);
                   
--- ISSUE with LARGE SNAPSHOTS 654K total
select extract(month from added_date) the_month
--, extract(year from added_date) the_year--, count(*)
FROM SALES_FORCE_EC_TEST
     WHERE SNAPSHOT_VERSION =
              (SELECT MAX (SNAPSHOT_VERSION) FROM SALES_FORCE_EC_TEST)
where  the_month = 12

group by extract(month from added_date) the_month, extract(year from added_date) the_year

select extract(month from sysdate) from dual

--views
select * from SALES_FORCE_EC_MODS_test

