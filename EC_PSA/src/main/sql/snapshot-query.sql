-- Added Date
-- Country Code
-- Transcript
-- Proof of Graduation
-- NLI
-- IRL
-- WR3 Account type (certification vs. profile)

--2017-05-15: TODO:  test this.  It may work as is and not require left joins. See -- below on CERTIFICATION type.

      INSERT INTO SALES_FORCE_EC (ROW_NUM,
                                  NCS_FOLDER,
                                  FIRST_NAME,
                                  LAST_NAME,
                                  SRF_DATE,
                                  EMAIL,
                                  AM_ENROLL_ID,
                                  ENROLL_SEMESTER,
                                  ENROLL_YEAR,
                                  OPEN_101_FORM,
                                  INTL_FLAG,
                                  home_school_staff,
                                  split_flag,
                                  cdn_flag,
                                  have_test_score,
                                  graduation_date,
                                  ready_to_process,
                                  min_task_date,
                                  max_task_date,
                                  sport_code,
                                  request_final_date,
                                  div1_decision_date,
                                  div1_sport_decision_id,
                                  div2_decision_date,
                                  div2_sport_decision_id,
                                  internal_sport_decision_id,
                                  recruit_cycle,
                                  cert_type_div1, cert_type_div2, pe_div1, pe_div2,
                                  payment_status,
                                  added_date,
                                  account_type,
                                  file_stat,
                                  billp_div1,billf_div1)
--SELECT                                   
            SELECT ROWNUM,
                innera.ncs_folder,
                innera.fname,
                innera.lname,
                innera.srf_date,
                innera.email,
                innera.am_enroll_id,
                innera.enroll_semester,
                innera.enroll_year,
                innera.open_101_form,
                innera.intl_flag,
                innera.home_school_staff,
                innera.split_flag,
                innera.cdn_flag,
                innera.have_test_score,
                innera.graduation_date,
                innera.ready_to_process,
                innera.min_task_date,
                innera.max_task_date,
                innerb.sport_code,
                innerb.request_final_date,
                innerb.div1_decision_date,
                innerb.div1_sport_decision_id,
                innerb.div2_decision_date,
                innerb.div2_sport_decision_id,
                innerb.internal_sport_decision_id,
                innerb.recruit_cycle,
                innera.cert_type_div1, innera.cert_type_div2, innera.pe_div1, innera.pe_div2,
                innera.payment_status,
                innera.added_date,
                innera.account_type,
                innera.file_stat,
                innera.billp_div1, innera.billf_div1
           FROM (
           
           SELECT s.ncs_folder,
                        s.fname,
                        s.lname,
                        s.srf_date,
                        s.email,
                        s.am_enroll_id,
                        ae.enroll_semester,
                        ae.enroll_year,
                        ae.open_101_form,
                        s.intl_flag,
                        s.home_school_staff,
                        s.split_flag,
                        s.cdn_flag,
                        s.test have_test_score,
                        s.graduation_date,
                        s.ready_to_process,
                        s.cert_type_div1, s.cert_type_div2, s.pe_div1, s.pe_div2,
                        t.min_task_date,
                        t.max_task_date,
                        s.payment_status,
                        s.added_date,
                        sa.account_type,
                        s.file_stat,
                        s.billp_div1, s.billf_div1
                   FROM student s
                        JOIN student_account sa on (s.ncs_folder = sa.ncs_folder)
                        JOIN am_enroll ae
                           ON (s.am_enroll_id = ae.am_enroll_id)
                        LEFT OUTER JOIN
                        (
                        select ncs_folder, min(date_task_assigned) min_task_date, max(date_task_assigned) max_task_date
                        from student_tasks where date_task_completed is null group by ncs_folder
                        ) t ON (t.ncs_folder = s.ncs_folder)
                  WHERE     active_ep_window (ae.am_enroll_id) = 'T'
                        AND s.email IS NOT NULL
                        --AND sa.account_type = 'CERTIFICATION' can't limit on this. 

                        ) 
                        innera
                -- Sport Cert Data
                JOIN
                (SELECT sp.ncs_folder,
                        sp.sport_code,
                        sp.request_final_date,
                        sc.div1_decision_date,
                        sc.div1_sport_decision_id,
                        sc.div2_decision_date,
                        sc.div2_sport_decision_id,
                        sc.internal_sport_decision_id,
                        i.recruit_cycle
                   FROM sport_participation sp
                        JOIN sport_certification sc
                           ON (    sp.ncs_folder = sc.ncs_folder
                               AND sp.sport_code = sc.sport_code)
                        LEFT OUTER JOIN
                        (  SELECT DISTINCT
                                  ncs_folder,
                                  sport,
                                  MAX (recruit_cycle) recruit_cycle
                             FROM irl
                            WHERE active = 'Y'
                         GROUP BY ncs_folder, sport) i
                           ON (    sp.ncs_folder = i.ncs_folder
                               AND sp.sport_code = i.sport)) innerb
                   ON (innera.ncs_folder = innerb.ncs_folder);


--OLD
            SELECT rownum, innera.ncs_folder,
                innera.srf_date,
                innera.email,
                innera.am_enroll_id,
                innera.enroll_semester,
                innera.enroll_year,
                innera.open_101_form,
                innera.intl_flag,
                innera.home_school_staff,
                innera.split_flag,
                innera.cdn_flag,
                innera.have_test_score,
                innera.graduation_date,
                innera.ready_to_process,
                innera.min_task_date,
                innera.max_task_date,
                innerb.sport_code,
                innerb.request_final_date,
                innerb.div1_decision_date,
                innerb.div1_sport_decision_id,
                innerb.div2_decision_date,
                innerb.div2_sport_decision_id,
                innerb.internal_sport_decision_id,
                innerb.recruit_cycle,
                innera.cert_type_div1, innera.cert_type_div2, innera.pe_div1, innera.pe_div2,
                innera.payment_status
           FROM (
           SELECT s.ncs_folder,
                        s.srf_date,
                        s.email,
                        s.am_enroll_id,
                        ae.enroll_semester,
                        ae.enroll_year,
                        ae.open_101_form,
                        s.intl_flag,
                        s.home_school_staff,
                        s.split_flag,
                        s.cdn_flag,
                        s.test have_test_score,
                        s.graduation_date,
                        s.ready_to_process,
                        s.cert_type_div1, s.cert_type_div2, s.pe_div1, s.pe_div2,
                        t.min_task_date,
                        t.max_task_date,
                        s.payment_status
                   FROM student s
                        JOIN am_enroll ae
                           ON (s.am_enroll_id = ae.am_enroll_id)
                        LEFT OUTER JOIN
                        (
                        select ncs_folder, min(date_task_assigned) min_task_date, max(date_task_assigned) max_task_date
                        from student_tasks where date_task_completed is null group by ncs_folder
                        ) t ON (t.ncs_folder = s.ncs_folder)
                  WHERE     active_ep_window (ae.am_enroll_id) = 'T'
                        AND s.email IS NOT NULL
                        ) innera
                -- Sport Cert Data
                JOIN
                (SELECT sp.ncs_folder,
                        sp.sport_code,
                        sp.request_final_date,
                        sc.div1_decision_date,
                        sc.div1_sport_decision_id,
                        sc.div2_decision_date,
                        sc.div2_sport_decision_id,
                        sc.internal_sport_decision_id,
                        i.recruit_cycle
                   FROM sport_participation sp
                        JOIN sport_certification sc
                           ON (    sp.ncs_folder = sc.ncs_folder
                               AND sp.sport_code = sc.sport_code)
                        LEFT OUTER JOIN
                        (  SELECT DISTINCT
                                  ncs_folder,
                                  sport,
                                  MAX (recruit_cycle) recruit_cycle
                             FROM irl
                            WHERE active = 'Y'
                         GROUP BY ncs_folder, sport) i
                           ON (    sp.ncs_folder = i.ncs_folder
                               AND sp.sport_code = i.sport)) innerb
                   ON (innera.ncs_folder = innerb.ncs_folder);
                   
  select * from student
  
  --just STUDENT table
           SELECT rownum,s.ncs_folder,
                        s.srf_date,
                        s.email,
                        s.am_enroll_id,
                        --ae.enroll_semester,
                        --ae.enroll_year,
                        --ae.open_101_form,
                        s.intl_flag,
                        s.home_school_staff,
                        s.split_flag,
                        s.cdn_flag,
                        s.test have_test_score,
                        s.graduation_date,
                        s.ready_to_process,
                        s.cert_type_div1, s.cert_type_div2, s.pe_div1, s.pe_div2,
                        --t.min_task_date,
                        --t.max_task_date,
                        s.payment_status,
                        s.added_date
                   FROM student s  
                   
                   