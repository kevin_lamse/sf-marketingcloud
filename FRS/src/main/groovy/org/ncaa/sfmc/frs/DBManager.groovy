package org.ncaa.sfmc.frs;

import groovy.sql.Sql;


import org.ncaa.sfmc.frs.entities.*;

import oracle.sql.CLOB;

/**

 */
class DBManager {

	def sql
	def jrc

	public DBManager(JobRunnerConfig jrc) {
		this.jrc = jrc
		sql = Sql.newInstance(
				jrc.dbConnection,
				jrc.dbUsername,
				jrc.dbPassword)
	}

	public List getFrsSalesforceEmailList() {
		List retList = new ArrayList()

		String sqlS = "select FRS_EMAIL_ID, STATUS from FRS_SALESFORCE_EMAIL where SENT_DATE IS NULL"
		println "SQL = "+sqlS
		sql.eachRow(sqlS)
				{ row ->

					SrfSalesforceEmail srfSalesforceEmail = new SrfSalesforceEmail()

					srfSalesforceEmail.FRS_EMAIL_ID 	= row.FRS_EMAIL_ID
					srfSalesforceEmail.STATUS 	= row.STATUS

					retList.add(srfSalesforceEmail)

				}
		return retList
	}

	/*
		String		PRIMARY_CONTACT_EMAIL
	String		EMAIL_TO
	String		EMAIL_SUBJECT
	String		EMAIL_BODY
	 */
	public List getView(String viewName) {
		List retList = new ArrayList()

		String sqlS = "select PRIMARY_CONTACT_EMAIL, EMAIL_TO, SUBJECT EMAIL_SUBJECT, BODY EMAIL_BODY from "+viewName
		println "SQL = "+sqlS
		sql.eachRow(sqlS)
		{ row ->

			SalesForceExport sfe = new SalesForceExport()
			sfe.PRIMARY_CONTACT_EMAIL = row.PRIMARY_CONTACT_EMAIL
			sfe.EMAIL_TO 		= row.EMAIL_TO
			sfe.EMAIL_SUBJECT 	= row.EMAIL_SUBJECT
			sfe.EMAIL_BODY 		= row.EMAIL_BODY
						
			retList.add(sfe)
			
		}
		return retList

	}

	public void updateToProcessTable(frsEmailId, viewToProcessSize) {
		println "updateToProcessTable( "+ frsEmailId+ ", " + viewToProcessSize + ") records!"
		try {
			sql.executeUpdate("update FRS_SALESFORCE_EMAIL set sent_date = (select sysdate from dual), SENT_COUNT = ? where frs_email_id = ?",[viewToProcessSize,frsEmailId])
		}catch (Exception e) {
			e.printStackTrace()
			jrc.logfile.append "Error in logSend: "+e.message()
		}
	}
	
/* Example
	public int getCommitteeViewCount() {
		println "getCommitteeViewCount()"
		int retVal = -1
		def s_ret = sql.rows(" select * from COMMITTEESALESFORCEV  ")
		retVal = s_ret.size();
		return retVal
	}
*/

	public static void main(String[] args){
		DBManager dbc = new DBManager();
		println "done"
	}

}
