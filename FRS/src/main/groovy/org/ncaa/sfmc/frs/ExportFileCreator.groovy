package org.ncaa.sfmc.frs;


import java.text.SimpleDateFormat

import org.ncaa.sfmc.frs.JobRunner;
import org.ncaa.sfmc.frs.JobRunnerConfig;
import org.ncaa.sfmc.frs.entities.*;


class ExportFileCreator {


	JobRunnerConfig jrc

	public ExportFileCreator(JobRunnerConfig jrc) {
		this.jrc = jrc
	}


	public String createExportFile(List list, String fileNameParam) {
		jrc.logfile.append "ExportFileCreator.createCommitteesTabDelim() count="+list.size() +"\n"
		String fileName = jrc.exportDir+ "/"+fileNameParam
		
		File f = new File(fileName)

		f.append("EMAIL_TO,EMAIL_SUBJECT,EMAIL_BODY\n")

		for(SalesForceExport e: list) {
			f.append(rv(e.EMAIL_TO)+","+rv(e.EMAIL_SUBJECT)+","+stripit(e.EMAIL_BODY) +"\n" )
		}

		if(jrc.sendTestEmail) {
			f.append(jrc.testEmail+","+fileNameParam+","+stripit(list.get(0).EMAIL_BODY)+"\n")
		}

		return fileName
	}

	public String createTrigger(String fileNameParam,String dateString) {
		jrc.logfile.append "ExportFileCreator.createTrigger() \n"
		String fileName = jrc.exportDir+ "/"+fileNameParam+".trigger"

		File f = new File(fileName)

		f.append("send file: ")
		f.append(dateString)
		return fileName
	}


	private String rvf(java.sql.Timestamp d) {
		String rval = ""

		if(d != null)
			rval = "\""+d.format("yyyy-MM-dd")+"\""
		return rval
	}
	private String rv(java.sql.Timestamp d) {
		String rval = ""

		if(d != null)
			rval = "\""+d+"\""
		return rval
	}

	private String rv(String s) {
		String rval = ""

		if(s != null)
			rval = "\""+ s +"\""
		return rval
	}

	private String rv(int s) {
		String rval = ""

		if(s != null)
			rval = "\""+ s +"\""
		return rval
	}

	private String rv2(String s) {
		String rval = ""

		if(s != null)
			rval = s
		return rval
	}

	private String stripit(String s) {
		String rval = ""

		if(s != null) {
			rval = s.replaceAll("\\n","")
			rval = rval.replaceAll("\\r","")
			rval = rval.replaceAll("\\t","")
			rval = "\""+ rval+ "\""
		}
		return rval
	}

}
