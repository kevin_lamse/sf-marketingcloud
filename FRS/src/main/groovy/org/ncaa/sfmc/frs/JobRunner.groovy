package org.ncaa.sfmc.frs;

import org.ncaa.sfcm.frs.entities.*;

import org.ncaa.sfmc.*;

import groovy.util.logging.*;

/**
 * Host Reporting Export
 * 
 * @author klamse
 *
 */
@Log4j2
class JobRunner {


	JobRunnerConfig jrc
	boolean testEnv = false

	public JobRunner() {
		this.jrc = new JobRunnerConfig()
	}


	public static void main(String[] args) {
		println "JobRunner.main() for TEMPLATE"
		
		Date startD = new Date()

		JobRunner jr = new JobRunner()
		
		jr.jrc.logfile.append("START: =============================================  \n")
		jr.jrc.logfile.append("START: "+startD.format("yyyy-MM-dd hh:mm:ss a") +"\n")
		jr.jrc.logfile.append("START: =============================================  \n")

		ExportFileCreator efc = new ExportFileCreator(jr.jrc)

		println "TODO: implement logic" 
		DBManager dbm = new DBManager(jr.jrc)
		Sftp sftp = new Sftp(jr.jrc)

		List frsSalesforceEmailList = dbm.getFrsSalesforceEmailList();

		println "frsSalesforceEmailList.size = " +frsSalesforceEmailList.size()

		frsSalesforceEmailList.each {
			println "it= " + it.STATUS
			List viewToProcess = dbm.getView(jr.jrc.viewByStatus.get(it.STATUS))
			int viewToProcessSize = viewToProcess.size()
			println "viewToProcess.size()= " + viewToProcessSize

			//view name, file and sftp dir the same.
			String fileName = jr.jrc.viewByStatus.get(it.STATUS)+".csv"
			String fileCreated = efc.createExportFile(viewToProcess,fileName)

			println "FileCreated = "+fileCreated

			sftp.sendFile(fileCreated, jr.jrc.viewByStatus.get(it.STATUS) )

			// Get list to send
			dbm.updateToProcessTable(it.FRS_EMAIL_ID, viewToProcessSize)

			// if immediate, send immediate file.
			jr.jrc.sendEmail("Please see that attached document to view the email list.",fileName)

		}

		Date endD = new Date()
		jr.jrc.logfile.append("\nEND: "+endD.format("yyyyMMdd hh:mm:ss a") +"\n")
		jr.jrc.logfile.append("======================================\n")

	}
}
