package org.ncaa.sfmc.frs;

import java.io.File;
import groovy.util.logging.Log4j2;

@Log4j2
class JobRunnerConfig {

	final String LOG_DIR = "logs/"
	String LOG_FILE = "SF_LOG";
	protected File logfile
	//File log
	String dateFormatted

	boolean createFile = true
	boolean sendFile = true
	boolean showDebug = true
	boolean sendTestEmail = false
	boolean sendSuccessEmail = false

	String testEmail
	String sendSuccessEmailAddress
	String viewName
	String configFile
	String env
	
	String exportDir
	String fileName
	String sftpURL
	String password
	AntBuilder ant

	Map viewByStatus = new HashMap();

	String dbConnection
	String dbUsername
	String dbPassword

	//env - prod or test
	public JobRunnerConfig(String env) {
		println "RUN_JOB_ENV ="+ System.getenv("RUN_JOB_ENV")
		env = System.getenv("RUN_JOB_ENV")
		setProperties(env)
		createLogfile(env)
		ant = new AntBuilder() //do I need so many?
		logfile.append "\nJobRunnerConfig() initialized \n"
	}

	private void createLogfile(String env) {
		Date d = new Date()
		dateFormatted = d.format("yyyyMMdd")
		println "dateFormatted= " + dateFormatted
		logfile = new File(this.LOG_DIR+env+"-"+this.LOG_FILE+"-"+dateFormatted+".log")
		log.info ("(log.info)start at {} ",dateFormatted) // does this work?

	}

	private void setProperties(String env) {
		String propFile = "src/main/resources/job-"+env+".properties"
		println "JobRunnerConfig.setProperties() from " +propFile
		Properties properties = new Properties()
		File propertiesFile = new File(propFile)
		propertiesFile.withInputStream { properties.load(it) }

		//configFile
		if(properties."sfmc.configFile")
			this.configFile = properties."sfmc.configFile"
		println "(JobRunnerConfig)this.createFile = "+this.configFile
		println "propertiesFile = "+propertiesFile

		if(properties."sfmc.createFile")
			this.createFile = properties."sfmc.createFile".toBoolean()
		println "(JobRunnerConfig)this.createFile = "+this.createFile

		if(properties."sfmc.sendFile")
			this.sendFile = properties."sfmc.sendFile".toBoolean()
		println "(JobRunnerConfig)this.sendFile = "+this.sendFile

		if(properties."sfmc.showDebug")
			this.showDebug = properties."sfmc.showDebug".toBoolean()
		println "(JobRunnerConfig)this.showDebug = "+this.showDebug

		//sfmc.sendTestEmail
		if(properties."sfmc.sendTestEmail")
			this.sendTestEmail = properties."sfmc.sendTestEmail".toBoolean()
		println "(JobRunnerConfig)this.sendTestEmail = "+this.sendTestEmail

		//testEmail
		if(properties."sfmc.testEmail")
			this.testEmail = properties."sfmc.testEmail"
		println "(JobRunnerConfig)this.testEmail = "+this.testEmail

		//sendSuccessEmail
		if(properties."sfmc.sendSuccessEmail")
			this.sendSuccessEmail = properties."sfmc.sendSuccessEmail".toBoolean()
		println "(JobRunnerConfig)this.sendSuccessEmail = "+this.sendSuccessEmail

		if(properties."sfmc.sendSuccessEmailAddress")
			this.sendSuccessEmailAddress = properties."sfmc.sendSuccessEmailAddress"
		println "(JobRunnerConfig)this.sendSuccessEmailAddress = "+this.sendSuccessEmailAddress

		if(properties."sfmc.exportDir")
			this.exportDir = properties."sfmc.exportDir"
		println "(JobRunnerConfig)this.exportDir = "+this.exportDir

		if(properties."sfmc.fileName")
			this.fileName = properties."sfmc.fileName"
		println "(JobRunnerConfig)this.fileName = "+this.fileName

		if(properties."sfmc.viewName")
			this.viewName = properties."sfmc.viewName"
		println "(JobRunnerConfig)this.viewName = "+this.viewName
		
		if(properties."sfmc.sftp.url")
			this.sftpURL = properties."sfmc.sftp.url"
		println "(JobRunnerConfig) this.sftpURL = "+this.sftpURL

		if(properties."sfmc.sftp.password")
			this.password= properties."sfmc.sftp.password"

		//status
		if(properties."sfmc.view.status.A")
			this.viewByStatus.put("A",properties."sfmc.view.status.A")
		println "(JobRunnerConfig) sfmc.view.status.A = "+viewByStatus.get("A")

		if(properties."sfmc.view.status.R")
			this.viewByStatus.put("R",properties."sfmc.view.status.R")
		println "(JobRunnerConfig) sfmc.view.status.R = "+viewByStatus.get("R")

		if(properties."sfmc.view.status.X")
			this.viewByStatus.put("X",properties."sfmc.view.status.X")
		println "(JobRunnerConfig) sfmc.view.status.X = "+viewByStatus.get("X")

		if(properties."sfmc.view.status.Y")
			this.viewByStatus.put("Y",properties."sfmc.view.status.Y")
		println "(JobRunnerConfig) sfmc.view.status.Y = "+viewByStatus.get("Y")

		//Load another file
		propFile = "src/main/resources/jdbc-"+env+".properties"
		println "JobRunnerConfig.setProperties() from " +propFile
		propertiesFile = new File(propFile)
		propertiesFile.withInputStream { properties.load(it) }
		if(properties."sfmc.dbConnection")
			this.dbConnection = properties."sfmc.dbConnection"
		println "(JobRunnerConfig) this.dbConnection = "+this.dbConnection

		if(properties."sfmc.dbUsername")
			this.dbUsername = properties."sfmc.dbUsername"
		println "(JobRunnerConfig) this.dbUsername = "+this.dbUsername

		if(properties."sfmc.dbPassword")
			this.dbPassword = properties."sfmc.dbPassword"

	}

	public void sendEmail(String messageS, String fileToAttach){
		println "message= "+messageS
		println "fileToAttach= "+fileToAttach
		if(this.sendSuccessEmail) {
			this.ant.mail(
					mailhost: 'localhost',
					mailport: '25',
					subject: 'FRS Email List Sent',
					charset: 'utf-8') {
				from(address: 'klamse@ncaa.org')
				to(address: this.sendSuccessEmailAddress)
				message(messageS)
				attachments() {
					fileset(dir: this.exportDir)
							{ include(name: fileToAttach) }
				}
			}

			this.logfile.append(" SEND EMAIL -> JobRunnerConfig.setEmail() to "+this.sendSuccessEmailAddress+"\n")
		} else {
			this.logfile.append(" SKIP EMAIL -> JobRunnerConfig.setEmail() to "+this.sendSuccessEmailAddress+"\n")
		}
	}

	public static void main(String[] args) {

		println "JobRunnerConfig.groovy"
		JobRunnerConfig jrc = new JobRunnerConfig()
		jrc.sendEmail("temp","temp")

	}

}
