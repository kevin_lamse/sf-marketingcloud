package org.ncaa.sfmc.frs;

import org.ncaa.sfmc.frs.JobRunnerConfig;

class Sftp {

	JobRunnerConfig jrc
	String toDir
	String password

	public Sftp(JobRunnerConfig jrc) {
		println "Sftp() initialized"
		this.jrc = jrc
	}

	public void sendFile(String fileToSend, String sftpdir ) {
		jrc.logfile.append " ->Sftp.sendFile "+ fileToSend
		String sftpUrl = jrc.sftpURL+"/"+sftpdir
		println " ->sending to: "+sftpUrl
		println " ->sendFile: "+jrc.sendFile // true/false
		if(jrc.sendFile) {
			jrc.ant.scp(
				file: fileToSend,
				todir: sftpUrl,
				password: jrc.password,
				sftp: true,
				trust: true,
				verbose: true
				)
		}
	}
}
