package org.ncaa.sfmc.frs.entities;

/**
 * Configure this class as needed for the export.
 * @author klamse
 *
 */
class SrfSalesforceEmail {

    Integer FRS_EMAIL_ID
    String STATUS
}