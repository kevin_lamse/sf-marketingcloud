
select * from FRS_SALESFORCE_EMAIL order by SENT_DATE DESC

select * from FRS_SALESFORCE_EMAIL where SENT_DATE is NULL

desc FRS_SALESFORCE_EMAIL

update FRS_SALESFORCE_EMAIL set sent_date = (select sysdate from dual), SENT_COUNT = 222
where frs_email_id = 11

insert into FRS_SALESFORCE_EMAIL(frs_email_id,status) values (1,'A')

insert into FRS_SALESFORCE_EMAIL(frs_email_id,status) values (1,'R')

insert into FRS_SALESFORCE_EMAIL(frs_email_id,status) values (1,'X')

insert into FRS_SALESFORCE_EMAIL(frs_email_id,status) values (1,'Y')

delete from FRS_SALESFORCE_EMAIL where status in ('X','R','A')

--sfmc.view.status.A=
--
-- --1726, OK
select * from FRS_SALESFORCEALL_V

--# If Status is R, the view you will use to send emails is FRS_SALESFORCEREM_V
--1427,
sfmc.view.status.R=
select * from FRS_SALESFORCEREM_V

# If Status is X, the view you will use to send emails is FRS_SALESFORCEALLTEST_V
--# SF: data extention working
-- 6, OK
sfmc.view.status.X=
select * from FRS_SALESFORCEALLTEST_V

# If Status is Y, the view you will use to send emails is FRS_SALESFORCEREMTEST_V
--# SF: data extension working
-- 3, OK
sfmc.view.status.Y=
select * from FRS_SALESFORCEREMTEST_V
