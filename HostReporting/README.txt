2017-04-27: Let's make this truely a  template.  Make everything generic and setup a test TEMPLATE_VIEW (table or view) for testing. 
1. Do all the config in the JobRunnerConfig file.
2. Point to TEST DB for testing. Set to IS_TEST in job.properites.
3. Allow TEST or PROD from same directory without changing config files.  Note: this has not been consistently developed.  Try to do better. 
4. Parameterize "everything". 
 - create job and job-test properites files.

V2
--elements of the table or view will be used for upload file. 
--NCAA Schema  	
DB Tables
create table SF_MARKETING_EMAILS(
	ORACLE_TABLE	VARCHAR2(30), -- append _TEST for test.  Include schema name if necessary. 
	SF_FILE_UPLOAD_DIR	VARCHAR(200), -- ADD THIS TO SFTP URL, append _TEST for test
	ACTIVE 			VARCAHR2(1) -- T/F
)
