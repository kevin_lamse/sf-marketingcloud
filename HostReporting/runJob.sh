
#!/bin/bash

args=("$@")
c=("$#")
if [ $c -lt 1 ]; then
        echo "Please specify: local, dev3, or prod"
fi

if [ ${args[0]} = "local" ]; then
        echo "run local..."
        export CRONJOB_BASEDIR=/Users/klamse/dev/ws-experiment/SF-MarketingCloud/HostReporting
        cd $CRONJOB_BASEDIR
		export RUN_JOB_ENV=test
        . ../setenv-local.sh

elif [ ${args[0]} = "test" ]; then
        echo "run test..."
        export CRONJOB_BASEDIR=/data/cronjobs/SF-MarketingCloud-develop/HostReporting
	export RUN_JOB_ENV=test
        cd $CRONJOB_BASEDIR
	export RUN_JOB_ENV=test
        . ../setenv-prod.sh

elif [ ${args[0]} = "prod" ]; then
        echo "run prod..."
        export CRONJOB_BASEDIR=/data/cronjobs/SF-MarketingCloud/HostReporting
	export RUN_JOB_ENV=prod
        cd $CRONJOB_BASEDIR
		export RUN_JOB_ENV=prod
        . ../setenv-prod.sh

fi

gradle runJob --stacktrace
