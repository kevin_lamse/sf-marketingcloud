package org.ncaa.sfmc.hostreporting;

import groovy.sql.Sql

import org.ncaa.sfmc.hostreporting.DBManager;
import org.ncaa.sfmc.hostreporting.JobRunner;
import org.ncaa.sfmc.hostreporting.JobRunnerConfig;
import org.ncaa.sfmc.hostreporting.entities.*;

import oracle.sql.CLOB;

/**

 */
class DBManager {

	def sql
	def jrc

	public DBManager(JobRunnerConfig jrc) {
		this.jrc = jrc
		sql = Sql.newInstance(
				jrc.dbConnection,
				jrc.dbUsername,
				jrc.dbPassword)
	}

		
	public List getView() {
		List retList = new ArrayList()

		String sqlS = "select EMAIL_TO, EMAIL_SUBJECT, EMAIL_BODY, EMAIL_FROM, EMAIL_CC, EMAIL_BCC from "+jrc.viewName
		println "SQL = "+sqlS
		sql.eachRow(sqlS)
		{ row ->

			SalesForceExport sfe = new SalesForceExport()
			sfe.EMAIL_TO 		= row.EMAIL_TO		
			sfe.EMAIL_SUBJECT 	= row.EMAIL_SUBJECT

			
			if(row.EMAIL_CC && row.EMAIL_CC.length() > 0) {
				
				sfe.EMAIL_BODY = "<br>This email was also sent to "+ row.EMAIL_CC + "</br>" + row.EMAIL_BODY
				
				SalesForceExport sfecc = new SalesForceExport()
				sfecc.EMAIL_TO = row.EMAIL_CC
				sfecc.EMAIL_SUBJECT 	= row.EMAIL_SUBJECT
				sfecc.EMAIL_BODY = "<br>This is a CC of an email sent to "+ row.EMAIL_TO + "</br>" + row.EMAIL_BODY
				retList.add(sfecc)
				
			} else { //no CC
				sfe.EMAIL_BODY	= row.EMAIL_BODY
			}

			if(row.EMAIL_BCC  && row.EMAIL_BCC.length() > 0) {
				SalesForceExport sfebcc = new SalesForceExport()
				sfebcc.EMAIL_TO = row.EMAIL_BCC
				sfebcc.EMAIL_SUBJECT 	= row.EMAIL_SUBJECT
				sfebcc.EMAIL_BODY = "<br>This is a BCC of an email sent to "+ row.EMAIL_TO + ". </br>" + row.EMAIL_BODY
				retList.add(sfebcc)
			} 
			
			retList.add(sfe)
			
		}
		return retList

	}

	public void logSend(int count) {
		println "TODO: logSend sent file with " + count + " records!"
		try {
			
		}catch (Exception e) {
			e.printStackTrace()
			jrc.logfile.append "Error in logSend: "+e.message()
		}
	}
	
/*	
	public int getCommitteeViewCount() {
		println "getCommitteeViewCount()"
		int retVal = -1
		def s_ret = sql.rows(" select * from COMMITTEESALESFORCEV  ")
		retVal = s_ret.size();
		return retVal
	}
*/

	public static void main(String[] args){
		DBManager dbc = new DBManager();
	//	println "test connection, studentCount = "+ dbc.getAnnouncementCount()
	//	dbc.getEmailList()
		println "done"
	}

}
