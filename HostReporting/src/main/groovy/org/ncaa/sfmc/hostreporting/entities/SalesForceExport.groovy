package org.ncaa.sfmc.hostreporting.entities

class SalesForceExport {

	String		EMAIL_TO
	String		EMAIL_SUBJECT
	String		EMAIL_BODY
	String 		EMAIL_FROM
	String 		EMAIL_CC
	String 		EMAIL_BCC
	String		BID_ID
	String 		RETURN_STATUS
	String 		BID_STATUS
	
}
