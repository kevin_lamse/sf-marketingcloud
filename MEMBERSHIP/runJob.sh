
#!/bin/bash

args=("$@")
c=("$#")
if [ $c -lt 1 ]; then
        echo "Please specify: local, dev3, or prod"
fi

if [ ${args[0]} = "local" ]; then
        echo "run local..."
        export CRONJOB_BASEDIR=/Users/klamse/dev/ws-experiment/SF-MarketingCloud/MEMBERSHIP
        cd $CRONJOB_BASEDIR
	export RUN_JOB_ENV=local
        . ../setenv-local.sh

elif [ ${args[0]} = "prodtest" ]; then
        echo "run dev3..."
        export CRONJOB_BASEDIR=/data/cronjobs/SF-MarketingCloud-develop/MEMBERSHIP
        cd $CRONJOB_BASEDIR
		export RUN_JOB_ENV=test
        . ../setenv-prod.sh

elif [ ${args[0]} = "prod" ]; then
        echo "run prod..."
        export CRONJOB_BASEDIR=/data/cronjobs/SF-MarketingCloud/MEMBERSHIP
        cd $CRONJOB_BASEDIR
		export RUN_JOB_ENV=prod
        . ../setenv-prod.sh

fi

gradle runJob --stacktrace
