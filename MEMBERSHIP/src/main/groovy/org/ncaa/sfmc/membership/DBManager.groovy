package org.ncaa.sfmc.membership;

import groovy.sql.Sql

import org.ncaa.sfmc.membership.JobRunnerConfig;
import org.ncaa.sfmc.membership.entities.*;


import oracle.sql.CLOB;

/**

 */
class DBManager {

	def sql
	def jrc
	def sqlNcaa

	public DBManager(JobRunnerConfig jrc) {
		this.jrc = jrc
		sql = Sql.newInstance(
				jrc.dbConnection,
				jrc.dbUsername,
				jrc.dbPassword)
		sqlNcaa = Sql.newInstance(
				jrc.dbConnectionNcaa,
				jrc.dbUsernameNcaa,
				jrc.dbPasswordNcaa)
	}

		
	public List getMemberList() {
		List membershipList = new ArrayList()
		
		int limit = 100
		int c = 0
		
		String sqlS ="""
                SELECT
                PERSON_ID, FIRST_NAME, LAST_NAME,
                   NAME_TITLE, ORG_ID, NAME_OFFICIAL,
                   EMAIL, ORG_TITLE, TITLE_CODE,
                   DESCRIPTION, DIVISION, SUBDIVISION,
                   CONF_ID, CONFERENCE, AUTONOMOUS_CONFERENCE, STATE,
                   SPORT_CODE, SPORT_DESCRIPTION
                FROM SF_MEMBER
                """ 
		if(!jrc.memberFullFile) {
			sqlS = sqlS + " where rownum <= " + limit
		}
				
		sql.eachRow(sqlS)
		{ row ->
				Member m = new Member()
				m.personId = row.person_id
				m.firstName = row.first_name
				m.lastName = row.last_name
				m.nameTitle = row.name_title
				m.orgId = row.org_id
				m.nameOfficial = row.name_official
				m.email = row.email
				m.orgTitle = row.org_title
				m.titleCode = row.title_code
				m.description = row.description
				m.division = row.division
				m.subdivision = row.subdivision
				m.confId = row.conf_id
				m.conference = row.conference
				m.autonomousConference = row.autonomous_conference
				m.state = row.state
				m.sportCode = row.sport_code
				m.sportDescription = row.sport_description
				
				membershipList.add(m)
				if(jrc.memberShow) {
					println ("Member ("+c+") = "+m)
					c++
				}	
		}
		
		return membershipList;
	}
	
	
	public List getMemberSportList() {
		
		List memberSportList = new ArrayList()
		int limit = 100
		int c = 0

		String sqlS = 
		"""
			SELECT
			ORG_ID, NAME_OFFICIAL, NAME_SORTED,
			   TYPE_ID, GEO_REGION, PROVISIONAL_YEAR,
			   RECLASS_START_YEAR, RECLASS_YEAR, RECLASS_DIVISION,
			   RECLASS_SUBDIVISION, DIVISION, SUBDIVISION,
			   ACRONYM, CONFERENCE_ID, AUTONOMOUS_CONFERENCE,
			   SPORT_CODE, SPORT_DIVISION, SPORT_SUBDIVISION,
			   SPORT_ACRONYM, SPORT_CONFERENCE, SPORT_RECLASS_YEAR,
			   SPORT_RECLASS_DIVISION, SPORT_RECLASS_SUBDIVISION, SPORT_REGION
			FROM LSDBI.SF_MEMBER_SPORT
                """
		if(!jrc.memberSportFullFile)
			sqlS = sqlS + " where rownum <= "+limit
			
		sql.eachRow(sqlS)
		{ row ->
				MemberSport m = new MemberSport()
				m.orgId = row.org_id
				m.nameOfficial = row.name_official
				m.nameSorted = row.name_sorted
				m.typeId = row.type_id
				m.geoRegion = row.geo_region
				m.provisionalYear = row.provisional_year
				m.reclassStartYear = row.reclass_start_year
				m.reclassYear = row.reclass_year
				m.reclassSubdivision = row.reclass_subdivision
				m.division = row.division

				m.subdivision = row.subdivision
				m.acronym = row.acronym
				m.conferenceId = row.conference_id
				m.autonomousConference = row.autonomous_conference

				m.sportCode = row.sport_code
				m.sportDivision = row.sport_division
				m.sportSubdivision = row.sport_subdivision
				m.sportAcronym = row.sport_acronym

				m.sportConference = row.sport_conference
				m.sportReclassYear = row.sport_reclass_year
				m.sportReclassDivision = row.sport_reclass_division
				m.sportRegion = row.sport_region

				memberSportList.add(m)
				if(jrc.memberSportShow) {
					println ("MemberSport ("+c+") = "+m)
					c++
				}
		}

		return memberSportList
	}
	
	public List getMemberCoachList() {
		List membershipList = new ArrayList()
		
		int limit = 100
		int c = 0
		
		String sqlS ="""
                SELECT
                PERSON_ID, FIRST_NAME, LAST_NAME,
                   NAME_TITLE, ORG_ID, NAME_OFFICIAL,
                   EMAIL, ORG_TITLE, TITLE_CODE,
                   DESCRIPTION, DIVISION, SUBDIVISION,
                   CONF_ID, CONFERENCE, AUTONOMOUS_CONFERENCE, STATE,
                   SPORT_CODE, SPORT_DESCRIPTION
                FROM SF_MEMBER_COACH
                """ 
		if(!jrc.memberFullFile) {
			sqlS = sqlS + " where rownum <= " + limit
		}
				
		sql.eachRow(sqlS)
		{ row ->
				Member m = new Member()
				m.personId = row.person_id
				m.firstName = row.first_name
				m.lastName = row.last_name
				m.nameTitle = row.name_title
				m.orgId = row.org_id
				m.nameOfficial = row.name_official
				m.email = row.email
				m.orgTitle = row.org_title
				m.titleCode = row.title_code
				m.description = row.description
				m.division = row.division
				m.subdivision = row.subdivision
				m.confId = row.conf_id
				m.conference = row.conference
				m.autonomousConference = row.autonomous_conference
				m.state = row.state
				m.sportCode = row.sport_code
				m.sportDescription = row.sport_description
				
				membershipList.add(m)
				if(jrc.memberCoachShow) {
					println ("MemberCoach ("+c+") = "+m)
					c++
				}
		}
		
		return membershipList;
	}

	public List getAnnouncementList() {
		List retList = new ArrayList()

		int limit = 50
		int c = 0
		String sqlS =
		"""
			SELECT EMAIL, EMAIL_TEXT FROM SALESFORCE_DAILY_LIST
        """

		if(!jrc.announcementFullFile)
			sqlS = sqlS + " where rownum <= "+limit
			
		sql.eachRow(sqlS)
		{ row ->
				Announcement a = new Announcement()
				a.email = row.email
				def clobValue = (oracle.sql.CLOB)row.email_text
				a.emailText = clobValue?.asciiStream.text

				retList.add(a)
				if(jrc.announcementShow) {
					println ("Announcement ("+c+") = "+a)
					c++
				}
		}

		return retList
	}

	//select email_address, first_name_common, last_name, group_name, department_name, short_title  from employee_list_2
	public List getEmployeeList() {
		List retList = new ArrayList()

		int limit = 50
		int c = 0
		String sqlS =
		"""
			select email_address, first_name_common, last_name, group_name, department_name, short_title  from employee_list_2
        """

		if(!jrc.employeeFullFile)
			sqlS = sqlS + " where rownum <= "+limit
			
		sqlNcaa.eachRow(sqlS)
		{ row ->
				Employee e = new Employee()
				e.email_address = row.email_address
				e.first_name	= row.first_name_common
				e.last_name		= row.last_name
				e.group			= row.group_name
				e.department	= row.department_name
				e.title			= row.short_title

				retList.add(e)
				if(jrc.announcementShow) {
					println ("Employee ("+c+") = "+e)
					c++
				}
		}

		return retList
	}


	public void buildYesterdaysList() {
		sql.call(" call SalesForce.build_yesterdays_list()")
	}

	public int getAnnouncementCount() {
		println "getAnnouncementCount()"
		int retVal = -1
		def s_ret = sql.rows(" select * from SALESFORCE_DAILY_LIST  ")
		retVal = s_ret.size();
		return retVal
	}

	public void logSend(int count) {
		println "TODO: logSend sent file with " + count + " records!"
		try {
			
		}catch (Exception e) {
			e.printStackTrace()
			jrc.logfile.append "Error in logSend: "+e.message()
		}
	}
	
	public static void main(String[] args){
		DBManager dbc = new DBManager();
		println "done"
	}

}
