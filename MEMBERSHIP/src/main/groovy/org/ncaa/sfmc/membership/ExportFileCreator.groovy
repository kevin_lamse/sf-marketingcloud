package org.ncaa.sfmc.membership;

import java.text.SimpleDateFormat

import org.ncaa.sfmc.membership.entities.*;


class ExportFileCreator {

	final String SUFFIX = ".csv";
	final String SUFFIX_TXT = ".txt";
	final String MEMBER_FILE_PREFIX = "MEMBER";
	final String MEMBER_SPORT_FILE_PREFIX = "MEMBER_SPORT"
	final String MEMBER_COACH_FILE_PREFIX = "MEMBER_COACH"
	final String ANNOUNCEMENT_FILE_PREFIX = "ANNOUNCEMENT"
	final String EMPLOYEE_FILE_PREFIX = "EMPLOYEE"
	final String EXPORT_DIR = "EXPORT/";
	
	JobRunnerConfig jrc

	public ExportFileCreator(JobRunnerConfig jrc) {
		this.jrc = jrc
	}

	public String createMemberCSV(List memberList) {
		println "createMemberCSV() "
		if(memberList)
				println "size= "+ memberList.size()

		String fileName = EXPORT_DIR+MEMBER_FILE_PREFIX+SUFFIX
		File f = new File(fileName)

		f.append( "PERSON_ID,FIRST_NAME,LAST_NAME,")
		f.append( "NAME_TITLE,ORG_ID,NAME_OFFICIAL,")
		f.append( "EMAIL,ORG_TITLE,TITLE_CODE,")
		f.append( "DESCRIPTION,DIVISION,SUBDIVISION,")
		f.append( "CONF_ID,CONFERENCE,AUTONOMOUS_CONFERENCE,STATE,")
		f.append( "SPORT_CODE,SPORT_DESCRIPTION\n")

		for(Member m : memberList) {
				f.append(rv(m.personId) +","+rv(m.firstName) +","+ rv(m.lastName) +",")
				f.append(rv(m.nameTitle) +","+rv(m.orgId) +","+rv(m.nameOfficial) +",")
				f.append(rv(m.email) +","+rv(m.orgTitle) +","+rv(m.titleCode) +",")
				f.append(rv(m.description) +","+rv(m.division) +","+rv(m.subdivision) +",")
				f.append(rv(m.confId) +","+rv(m.conference) +","+rv(m.autonomousConference) +","+rv(m.state)+",")
				f.append(rv(m.sportCode) +","+rv(m.sportDescription)+ "\n")

		}
		return fileName
	}

	public String createMemberSportCSV(List list) {
		println "createMemberSportCSV()"
		String fileName = EXPORT_DIR+MEMBER_SPORT_FILE_PREFIX+SUFFIX
		File f = new File(fileName)

		f.append("ORG_ID,NAME_OFFICIAL,NAME_SORTED,")
		f.append("TYPE_ID,GEO_REGION,PROVISIONAL_YEAR,")
		f.append("RECLASS_START_YEAR,RECLASS_YEAR,RECLASS_DIVISION,")
		f.append("RECLASS_SUBDIVISION,DIVISION,SUBDIVISION,")
		f.append("ACRONYM,CONFERENCE_ID,AUTONOMOUS_CONFERENCE,")
		f.append("SPORT_CODE,SPORT_DIVISION,SPORT_SUBDIVISION,")
		f.append("SPORT_ACRONYM,SPORT_CONFERENCE,SPORT_RECLASS_YEAR,")
		f.append("SPORT_RECLASS_DIVISION,SPORT_RECLASS_SUBDIVISION,SPORT_REGION\n")

		for(MemberSport ms : list) {
				f.append(rv(ms.orgId)+","+rv(ms.nameOfficial)+","+rv(ms.nameSorted)+",")
				f.append(rv(ms.typeId)+","+rv(ms.geoRegion)+","+rv(ms.provisionalYear)+",")
				f.append(rv(ms.reclassStartYear)+","+rv(ms.reclassYear)+","+rv(ms.reclassDivision)+",")
				f.append(rv(ms.reclassSubdivision)+","+rv(ms.division)+","+rv(ms.subdivision)+",")
				f.append(rv(ms.acronym)+","+rv(ms.conferenceId)+","+rv(ms.autonomousConference)+",")
				f.append(rv(ms.sportCode)+","+rv(ms.sportDivision)+","+rv(ms.sportSubdivision)+",")
				f.append(rv(ms.sportAcronym)+","+rv(ms.sportConference)+","+rv(ms.sportReclassYear)+",")
				f.append(rv(ms.sportReclassDivision)+","+rv(ms.sportReclassSubdivision)+","+rv(ms.sportRegion)+"\n")
		}

		return fileName
	}
		
	public String createMemberCoachCSV(List memberList) {
		println "createMemberCSV() "
		if(memberList)
				println "size= "+ memberList.size()

		String fileName = EXPORT_DIR+MEMBER_COACH_FILE_PREFIX+SUFFIX
		File f = new File(fileName)

		f.append( "PERSON_ID,FIRST_NAME,LAST_NAME,")
		f.append( "NAME_TITLE,ORG_ID,NAME_OFFICIAL,")
		f.append( "EMAIL,ORG_TITLE,TITLE_CODE,")
		f.append( "DESCRIPTION,DIVISION,SUBDIVISION,")
		f.append( "CONF_ID,CONFERENCE,AUTONOMOUS_CONFERENCE,STATE,")
		f.append( "SPORT_CODE,SPORT_DESCRIPTION\n")

		for(Member m : memberList) {
				f.append(rv(m.personId) +","+rv(m.firstName) +","+ rv(m.lastName) +",")
				f.append(rv(m.nameTitle) +","+rv(m.orgId) +","+rv(m.nameOfficial) +",")
				f.append(rv(m.email) +","+rv(m.orgTitle) +","+rv(m.titleCode) +",")
				f.append(rv(m.description) +","+rv(m.division) +","+rv(m.subdivision) +",")
				f.append(rv(m.confId) +","+rv(m.conference) +","+rv(m.autonomousConference) +","+rv(m.state)+",")
				f.append(rv(m.sportCode) +","+rv(m.sportDescription)+ "\n")

		}
		return fileName
	}

	
	public String createAnnouncementTabDelim(List list) {
		println "createAnnouncementTabDelim()"
		String fileName = EXPORT_DIR+ANNOUNCEMENT_FILE_PREFIX+"-"+jrc.dateFormatted+SUFFIX_TXT
		File f = new File(fileName)

		f.append("EMAIL\tEMAIL_TEXT \n")

		for(Announcement a : list) {
				f.append(rv2(a.email)+"\t"+stripit(a.emailText)+" \n")
		}

		return fileName
	}

	public String createEmployeeCSV(List list) {
		println "createEmployeeCSV()"
		String fileName = EXPORT_DIR+EMPLOYEE_FILE_PREFIX+SUFFIX
		File f = new File(fileName)

		f.append("EMAIL,FIRST_NAME,LAST_NAME,GROUP,DEPARTMENT,SHORT_TITLE\n")

		for(Employee e : list) {
				f.append(rv(e.email_address)+","+rv(e.first_name)+","+rv(e.last_name)+","+rv(e.group)+","+rv(e.department)+","+rv(e.title)+"\n")
		}

		return fileName
	}
	public String createExportFile(List list) {
		jrc.logfile.append "ExportFileCreator.createCommitteesTabDelim() count="+list.size() +"\n"
		String fileName = jrc.exportDir+ "/"+jrc.fileName
		
		File f = new File(fileName)

		f.append("EMAIL_TO,EMAIL_SUBJECT,EMAIL_BODY\n")

		for(SalesForceExport e: list) {
			f.append(rv(e.EMAIL_TO)+","+rv(e.EMAIL_SUBJECT)+","+stripit(e.EMAIL_BODY) +"\n" )
		}
		
		return fileName
	}


	private String rvf(java.sql.Timestamp d) {
		String rval = ""

		if(d != null)
			rval = "\""+d.format("yyyy-MM-dd")+"\""
		return rval
	}
	private String rv(java.sql.Timestamp d) {
		String rval = ""

		if(d != null)
			rval = "\""+d+"\""
		return rval
	}

	private String rv(String s) {
		String rval = ""
		if(s != null) {
			s = s.replaceAll("\"","'")
			rval = "\""+ s +"\""
		}
		return rval
	}

	private String rv(int s) {
		String rval = ""

		if(s != null)
			rval = "\""+ s +"\""
		return rval
	}

	private String rv2(String s) {
		String rval = ""

		if(s != null)
			rval = s
		return rval
	}

	private String stripit(String s) {
		String rval = ""

		if(s != null) {
			rval = s.replaceAll("\\n","")
			rval = rval.replaceAll("\\r","")
			rval = rval.replaceAll("\\t","")
			rval = "\""+ rval+ "\""
		}
		return rval
	}

}
