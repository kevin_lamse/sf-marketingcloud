package org.ncaa.sfmc.membership;

import org.ncaa.sfmc.membership.entities.SalesForceExport;



import groovy.util.logging.*;

/**
 * Host Reporting Export
 * 
 * @author klamse
 *
 */
@Log4j2
class JobRunner {


	JobRunnerConfig jrc
	boolean testEnv = false

	public JobRunner() {
		this.jrc = new JobRunnerConfig()
	}


	public static void main(String[] args) {
		println "JobRunner.main() for MEMBERSHIP"
		
		Date startD = new Date()

		JobRunner jr = new JobRunner()
		
		jr.jrc.logfile.append("START: =============================================  \n")
		jr.jrc.logfile.append("START: "+startD.format("yyyyMMdd hh:mm:ss a") +"\n")
		jr.jrc.logfile.append("START: =============================================  \n")

		ExportFileCreator efc = new ExportFileCreator(jr.jrc)

		println "TODO: implement logic" 
		DBManager dbm = new DBManager(jr.jrc)
		Sftp sftp = new Sftp(jr.jrc)

		//Member
		List memberList
		String memberFile
		if(jr.jrc.memberCreateFile) {
			memberList = dbm.getMemberList()
			println "next create file"
			memberFile = efc.createMemberCSV(memberList)
			jr.jrc.logfile.append("CREATE memberFile "+memberFile +"\n")

			sftp.sendFile(memberFile, jr.jrc.memberSftUrl, jr.jrc.memberSendFile)			
		}

		//MemberSport
		List memberSportList
		String memberSportFile
		if(jr.jrc.memberSportCreateFile) {
			memberSportList = dbm.getMemberSportList()
			println "next create file"
			memberSportFile = efc.createMemberSportCSV(memberSportList)
			jr.jrc.logfile.append("CREATE memberSportFile "+memberSportFile +"\n")

			sftp.sendFile(memberSportFile, jr.jrc.memberSportSftUrl, jr.jrc.memberSportSendFile)
		}
		
		//MemberCoach
		List memberCoachList
		String memberCoachFile
		if(jr.jrc.memberCoachCreateFile) {
			memberCoachList = dbm.getMemberCoachList()
			println "next create file"
			memberCoachFile = efc.createMemberCoachCSV(memberCoachList)
			jr.jrc.logfile.append("CREATE memberCoachFile "+memberCoachFile +"\n")

			sftp.sendFile(memberCoachFile, jr.jrc.memberCoachSftUrl, jr.jrc.memberCoachSendFile)
		}

		//Announcement
		List announcementList
		String announcementFile
		if(jr.jrc.announcementCreateFile) {
			if(jr.jrc.announcementRebuild) dbm.buildYesterdaysList
			
			int aCount = dbm.getAnnouncementCount()
			
			if(aCount > 0) {
				announcementList = dbm.getAnnouncementList()
				println "next create file"
				announcementFile = efc.createAnnouncementTabDelim(announcementList)
				jr.jrc.logfile.append("CREATE announcementFile "+announcementFile +"\n")

				sftp.sendFile(announcementFile, jr.jrc.announcementSftUrl, jr.jrc.announcementSendFile)
				
			} else {
				jr.jrc.logfile.append("No new announcements.")
			}
		}

		//Employee
		List employeeList
		String employeeFile
		if(jr.jrc.employeeCreateFile) {
			employeeList = dbm.getEmployeeList()
			println "next create file"
			employeeFile = efc.createEmployeeCSV(employeeList)
			jr.jrc.logfile.append("CREATE employeeFile "+employeeFile +"\n")

			sftp.sendFile(employeeFile, jr.jrc.employeeSftUrl, jr.jrc.employeeSendFile)
		}
		
		/*
		dbm.logSend(viewList.size())
		*/
		Date endD = new Date()
		jr.jrc.logfile.append("END: "+endD.format("yyyyMMdd hh:mm:ss a") +"\n")
		jr.jrc.logfile.append("======================================")

	}
}
