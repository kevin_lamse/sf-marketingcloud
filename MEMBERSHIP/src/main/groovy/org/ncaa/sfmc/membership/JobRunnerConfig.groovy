package org.ncaa.sfmc.membership;

import java.io.File

import groovy.util.logging.Log4j2;;

@Log4j2
class JobRunnerConfig {

	final String LOG_DIR = "logs/"
	String LOG_FILE = "SF_LOG";
	protected File logfile
	//File log
	String dateFormatted

	boolean memberCreateFile = true
	boolean memberSendFile = true
	boolean memberShow = true
	String memberSftUrl
	boolean memberFullFile = false

	boolean memberSportCreateFile = false
	boolean memberSportSendFile = false
	boolean memberSportShow = false
	String memberSportSftUrl
	boolean memberSportFullFile = false

	boolean memberCoachCreateFile = false
	boolean memberCoachSendFile = false
	boolean memberCoachShow = false
	String memberCoachSftUrl
	boolean memberCoachFullFile = false

	boolean announcementRebuild = false
	boolean announcementCreateFile = false
	boolean announcementSendFile = false
	boolean announcementShow = false
	String  announcementSftUrl
	boolean announcementFullFile = false

	boolean employeeCreateFile = false
	boolean employeeSendFile = false
	boolean employeeShow = false
	String  employeeSftUrl
	boolean employeeFullFile = false

	String viewName
	String configFile
	String env
	
	String exportDir
	String fileName
	String sftpURL
	String sftpPassword
	AntBuilder ant


	String dbConnection
	String dbUsername
	String dbPassword

	String dbConnectionNcaa
	String dbUsernameNcaa
	String dbPasswordNcaa

	//env - prod or test
	public JobRunnerConfig(String env) {
		println "RUN_JOB_ENV ="+ System.getenv("RUN_JOB_ENV")
		env = System.getenv("RUN_JOB_ENV")
		setProperties(env)
		createLogfile(env)
		ant = new AntBuilder() //do I need so many?
		logfile.append "\nJobRunnerConfig() initialized \n"
	}

	private void createLogfile(String env) {
		Date d = new Date()
		dateFormatted = d.format("yyyyMMdd")
		println "dateFormatted= " + dateFormatted
		logfile = new File(this.LOG_DIR+env+"-"+this.LOG_FILE+"-"+dateFormatted+".log")
		log.info ("(log.info)start at {} ",dateFormatted) // does this work?

	}

	private void setProperties(String env) {
		String propFile = "src/main/resources/job-"+env+".properties"
		println "JobRunnerConfig.setProperties() from " +propFile
		Properties properties = new Properties()
		File propertiesFile = new File(propFile)
		propertiesFile.withInputStream { properties.load(it) }

		//configFile
		if(properties."salesforce.configFile")
			this.configFile = properties."salesforce.configFile"
		println "(JobRunnerConfig)this.createFile = "+this.configFile
		println "propertiesFile = "+propertiesFile

		
		//member
				
		if(properties."salesforce.member.createFile")
			this.memberCreateFile = properties."salesforce.member.createFile".toBoolean()
		println "(JobRunnerConfig)this.memberCreateFile = "+this.memberCreateFile
		
		if(properties."salesforce.member.send")
			this.memberSendFile = properties."salesforce.member.send".toBoolean()
		println "(JobRunnerConfig)this.memberSendFile = "+this.memberSendFile

		if(properties."salesforce.member.show")
			this.memberShow = properties."salesforce.member.show".toBoolean()
		println "(JobRunnerConfig)this.memberShow = "+this.memberShow

		if(properties."salesforce.member.sftp.url")
			this.memberSftUrl = properties."salesforce.member.sftp.url"
		println "(JobRunnerConfig)this.memberSftUrl = "+this.memberSftUrl

		if(properties."salesforce.member.fullFile")
			this.memberFullFile = properties."salesforce.member.fullFile".toBoolean()
		println "(JobRunnerConfig)this.memberFillFile = "+this.memberFullFile
//--- memberSport
		if(properties."salesforce.memberSport.createFile")
			this.memberSportCreateFile = properties."salesforce.memberSport.createFile".toBoolean()
		println "(JobRunnerConfig)this.memberSportCreateFile = "+this.memberSportCreateFile
		
		if(properties."salesforce.memberSport.send")
			this.memberSportSendFile = properties."salesforce.memberSport.send".toBoolean()
		println "(JobRunnerConfig)this.memberSportSendFile = "+this.memberSportSendFile
	
		if(properties."salesforce.memberSport.show")
			this.memberSportShow = properties."salesforce.memberSport.show".toBoolean()
		println "(JobRunnerConfig)this.memberSportShow = "+this.memberSportShow
	
		if(properties."salesforce.memberSport.sftp.url")
			this.memberSportSftUrl = properties."salesforce.memberSport.sftp.url"
		println "(JobRunnerConfig)this.memberSportSftUrl = "+this.memberSportSftUrl
	
		if(properties."salesforce.memberSport.fullFile")
			this.memberSportFullFile = properties."salesforce.memberSport.fullFile".toBoolean()
		println "(JobRunnerConfig)this.memberSportFullFile = "+this.memberSportFullFile
//coach
		if(properties."salesforce.memberCoach.createFile")
			this.memberCoachCreateFile = properties."salesforce.memberCoach.createFile".toBoolean()
		println "(JobRunnerConfig)this.memberCoachCreateFile = "+this.memberCoachCreateFile
		
		if(properties."salesforce.memberCoach.send")
			this.memberCoachSendFile = properties."salesforce.memberCoach.send".toBoolean()
		println "(JobRunnerConfig)this.memberCoachSendFile = "+this.memberCoachSendFile
	
		if(properties."salesforce.memberCoach.show")
			this.memberCoachShow = properties."salesforce.memberCoach.show".toBoolean()
		println "(JobRunnerConfig)this.memberCoachShow = "+this.memberCoachShow
	
		if(properties."salesforce.memberCoach.sftp.url")
			this.memberCoachSftUrl = properties."salesforce.memberCoach.sftp.url"
		println "(JobRunnerConfig)this.memberCoachSftUrl = "+this.memberCoachSftUrl
	
		if(properties."salesforce.memberCoach.fullFile")
			this.memberCoachFullFile = properties."salesforce.memberCoach.fullFile".toBoolean()
		println "(JobRunnerConfig)this.memberCoachFillFile = "+this.memberCoachFullFile
//announce
		if(properties."salesforce.announcement.rebuild")
			this.announcementRebuild = properties."salesforce.announcement.rebuild".toBoolean()
		println "(JobRunnerConfig)this.announcementRebuild = "+this.announcementRebuild

		if(properties."salesforce.announcement.createFile")
			this.announcementCreateFile = properties."salesforce.announcement.createFile".toBoolean()
		println "(JobRunnerConfig)this.announcementCreateFile = "+this.announcementCreateFile
		
		if(properties."salesforce.announcement.send")
			this.announcementSendFile = properties."salesforce.announcement.send".toBoolean()
		println "(JobRunnerConfig)this.announcementSendFile = "+this.announcementSendFile
	
		if(properties."salesforce.announcement.show")
			this.announcementShow = properties."salesforce.announcement.show".toBoolean()
		println "(JobRunnerConfig)this.announcementShow = "+this.announcementShow
	
		if(properties."salesforce.announcement.sftp.url")
			this.announcementSftUrl = properties."salesforce.announcement.sftp.url"
		println "(JobRunnerConfig)this.announcementSftUrl = "+this.announcementSftUrl
	
		if(properties."salesforce.announcement.fullFile")
			this.announcementFullFile = properties."salesforce.announcement.fullFile".toBoolean()
		println "(JobRunnerConfig)this.announcementFillFile = "+this.announcementFullFile

		if(properties."salesforce.employee.createFile")
			this.employeeCreateFile = properties."salesforce.employee.createFile".toBoolean()
		println "(JobRunnerConfig)this.employeeCreateFile = "+this.employeeCreateFile
		
		if(properties."salesforce.employee.send")
			this.employeeSendFile = properties."salesforce.employee.send".toBoolean()
		println "(JobRunnerConfig)this.employeeSendFile = "+this.employeeSendFile
	
		if(properties."salesforce.employee.show")
			this.employeeShow = properties."salesforce.employee.show".toBoolean()
		println "(JobRunnerConfig)this.employeeShow = "+this.employeeShow
	
		if(properties."salesforce.employee.sftp.url")
			this.employeeSftUrl = properties."salesforce.employee.sftp.url"
		println "(JobRunnerConfig)this.aemployeeSftUrl = "+this.employeeSftUrl
	
		if(properties."salesforce.employee.fullFile")
			this.employeeFullFile = properties."salesforce.employee.fullFile".toBoolean()
		println "(JobRunnerConfig)this.employeeFillFile = "+this.employeeFullFile

				
		//not sure if we need this. 
		if(properties."salesforce.viewName")
			this.viewName = properties."salesforce.viewName"
		println "(JobRunnerConfig)this.viewName = "+this.viewName
		
		if(properties."salesforce.sftp.password")
			this.sftpPassword= properties."salesforce.sftp.password"

		//Load another file
		propFile = "src/main/resources/jdbc-"+env+".properties"
		println "JobRunnerConfig.setProperties() from " +propFile
		propertiesFile = new File(propFile)
		propertiesFile.withInputStream { properties.load(it) }
		if(properties."salesforce.dbConnection")
			this.dbConnection = properties."salesforce.dbConnection"
		println "(JobRunnerConfig) this.dbConnection = "+this.dbConnection

		if(properties."salesforce.dbUsername")
			this.dbUsername = properties."salesforce.dbUsername"
		println "(JobRunnerConfig) this.dbUsername = "+this.dbUsername

		if(properties."salesforce.dbPassword")
			this.dbPassword = properties."salesforce.dbPassword"

		if(properties."salesforce.ncaa.dbConnection")
			this.dbConnectionNcaa = properties."salesforce.ncaa.dbConnection"
		println "(JobRunnerConfig) this.dbConnectionNcaa = "+this.dbConnectionNcaa

		if(properties."salesforce.ncaa.dbUsername")
			this.dbUsernameNcaa = properties."salesforce.ncaa.dbUsername"
		println "(JobRunnerConfig) this.dbUsernameNcaa = "+this.dbUsernameNcaa

		if(properties."salesforce.ncaa.dbPassword")
			this.dbPasswordNcaa = properties."salesforce.ncaa.dbPassword"

	}

}
