package org.ncaa.sfmc.membership;

import org.ncaa.sfmc.membership.JobRunnerConfig;

class Sftp {

	JobRunnerConfig jrc
	String toDir
	String password

	public Sftp(JobRunnerConfig jrc) {
		println "Sftp() initialized"
		this.jrc = jrc
	}
	
	public void sendFile(String fileToSend, String toFtpDir, boolean sendFile ) {
		jrc.logfile.append " ->Sftp.sendFile "+ fileToSend + " to " + toFtpDir + " Send? "+sendFile
		println " ->sendFile: "+ fileToSend
		println " ->sending to: "+toFtpDir
		if(sendFile){
			println "sending..."
			jrc.ant.scp(
				file: fileToSend,
				todir: toFtpDir,
				password: jrc.sftpPassword,
				sftp: true,
				trust: true,
				verbose: true
				)
		} else {
			println "not sending..."
		}
	}
}
