package org.ncaa.sfmc.membership.entities

import groovy.transform.ToString;

@ToString
public class Announcement {

		String email
		String emailText
}