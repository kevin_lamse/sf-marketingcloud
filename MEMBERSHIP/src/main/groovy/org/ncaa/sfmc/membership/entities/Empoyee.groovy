package org.ncaa.sfmc.membership.entities

import groovy.transform.ToString;

@ToString
class Employee {

	String email_address
	String first_name
	String last_name
	String group
	String department
	String title
}
