package org.ncaa.sfmc.membership.entities


import groovy.transform.ToString;

@ToString
public class Member {

	/*
SELECT
PERSON_ID, FIRST_NAME, LAST_NAME,
   NAME_TITLE, ORG_ID, NAME_OFFICIAL,
   EMAIL, ORG_TITLE, TITLE_CODE,
   DESCRIPTION, DIVISION, SUBDIVISION,
   CONF_ID, CONFERENCE, AUTONOMOUS_CONFERENCE,
   SPORT_CODE, SPORT_DESCRIPTION
FROM NCAA.SF_MEMBERSHIP;
	 */

	Integer personId
		String firstName
		String lastName
		String nameTitle
		String orgId
		String nameOfficial
		String email
		String orgTitle
		String titleCode
		String description
		String division
		String subdivision
		String confId
		String Conference
		String autonomousConference
		String sportCode
		String sportDescription
		String state

		/*
		public String toString() {
				return "personId= "+personId +", email= "+email+ ", orgId= "+orgId
		}
		*/


}
