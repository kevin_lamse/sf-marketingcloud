package org.ncaa.sfmc.membership.entities


import groovy.transform.ToString;

@ToString
class MemberSport {

        Integer orgId
        String nameOfficial
        String nameSorted
        String typeId
        String geoRegion
        String provisionalYear
        String reclassStartYear
        String reclassYear
        String reclassDivision
        String reclassSubdivision
        String division
        String subdivision
        String acronym
        String conferenceId
        String autonomousConference
        String sportCode
        String sportDivision
        String sportSubdivision
        String sportAcronym
        String sportConference
        String sportReclassYear
        String sportReclassDivision
        String sportReclassSubdivision
        String sportRegion
}