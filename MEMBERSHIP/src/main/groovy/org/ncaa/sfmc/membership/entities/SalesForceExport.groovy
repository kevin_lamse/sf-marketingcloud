package org.ncaa.sfmc.membership.entities;

/**
 * Configure this class as needed for the export.
 * @author klamse
 *
 */
class SalesForceExport {

	String		EMAIL_TO
	String		EMAIL_SUBJECT
	String		EMAIL_BODY
	String 		EMAIL_FROM
	String 		EMAIL_CC
	String 		EMAIL_BCC
	String		BID_ID
	String 		RETURN_STATUS
	String 		BID_STATUS
	
}
