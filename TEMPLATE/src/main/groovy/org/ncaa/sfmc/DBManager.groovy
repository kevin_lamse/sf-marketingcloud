package org.ncaa.sfmc;

import groovy.sql.Sql;


import org.ncaa.sfmc.entities.*;

import oracle.sql.CLOB;

/**

 */
class DBManager {

	def sql
	def jrc

	public DBManager(JobRunnerConfig jrc) {
		this.jrc = jrc
		sql = Sql.newInstance(
				jrc.dbConnection,
				jrc.dbUsername,
				jrc.dbPassword)
	}

		
	public List getView() {
		List retList = new ArrayList()

		String sqlS = "select EMAIL_TO, EMAIL_SUBJECT, EMAIL_BODY, EMAIL_FROM, EMAIL_CC, EMAIL_BCC from "+jrc.viewName
		println "SQL = "+sqlS
		sql.eachRow(sqlS)
		{ row ->

			SalesForceExport sfe = new SalesForceExport()
			sfe.EMAIL_TO 		= row.EMAIL_TO
			sfe.EMAIL_SUBJECT 	= row.EMAIL_SUBJECT
			sfe.EMAIL_BODY 		= row.EMAIL_BODY
						
			retList.add(sfe)
			
		}
		return retList

	}

	public void logSend(int count) {
		println "TODO: logSend sent file with " + count + " records!"
		try {
			
		}catch (Exception e) {
			e.printStackTrace()
			jrc.logfile.append "Error in logSend: "+e.message()
		}
	}
	
/* Example
	public int getCommitteeViewCount() {
		println "getCommitteeViewCount()"
		int retVal = -1
		def s_ret = sql.rows(" select * from COMMITTEESALESFORCEV  ")
		retVal = s_ret.size();
		return retVal
	}
*/

	public static void main(String[] args){
		DBManager dbc = new DBManager();
		println "done"
	}

}
