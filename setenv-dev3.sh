
#For 10-7 env
export GROOVY_HOME=/opt/ncaa/java/groovy-2.4.7/
export JAVA_HOME=/opt/ncaa/java/jdk1.7.0_71/
export ANT_HOME=/opt/ncaa/java/apache-ant-1.9.4
export GRADLE_HOME=/opt/ncaa/java/gradle-3.1

export PATH=$GROOVY_HOME/bin:$JAVA_HOME/bin:$ANT_HOME/bin:$GRADLE_HOME/bin:$PATH
export CLASSPATH=$CLASSPATH:/opt/ncaa/jars/ojdbc6.jar

