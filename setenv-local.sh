
export T7=/Users/klamse/dev/servers/apache-tomcat-7.0.69
export JARS=/Users/klamse/dev/java/jars
export GRADLE_HOME=/Users/klamse/dev/java/gradle-2.14
export GROOVY_HOME=/Users/klamse/dev/java/groovy-2.4.7
export RUBY_HOME=/usr/local/Cellar/ruby/2.3.1

#Oracle
export VERSION=12.1.0.2.0
export ARCH=x86_64
export ORACLE_HOME=/Users/klamse/dev/dbs/instantclient_12_1
export LD_LIBRARY_PATH=$ORACLE_HOME
export DYLD_LIBRARY_PATH=$ORACLE_HOME
export DYLD_FALLBACK_LIBRARY_PATH=$ORACLE_HOME
export OCI_DIR=$ORACLE_HOME
export SQLPLUS_SCRIPTS=/Users/klamse/dev/scripts/sqlplus

export PATH=$GRADLE_HOME/bin:$GROOVY_HOME/bin:$RUBY_HOME/bin:$ORACLE_HOME:$SQLPLUS_SCRIPTS:$PATH

export RUN_JOB_ENV=test
