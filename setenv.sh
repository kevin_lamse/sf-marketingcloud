
export GROOVY_HOME=/data/EC/java/groovy-2.3.9
export JAVA_HOME=/data/EC/java/jdk1.7.0_71
export ANT_HOME=/data/EC/java/apache-ant-1.8.1/
export GRADLE_HOME=/data/EC/java/gradle-2.3

export PATH=$GROOVY_HOME/bin:$JAVA_HOME/bin:$ANT_HOME/bin:$GRADLE_HOME/bin:$PATH
export CLASSPATH=$CLASSPATH:/opt/ncaa/jars/ojdbc6.jar


