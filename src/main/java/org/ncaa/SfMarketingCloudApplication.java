package org.ncaa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SfMarketingCloudApplication {

	public static void main(String[] args) {
		SpringApplication.run(SfMarketingCloudApplication.class, args);
	}
}
